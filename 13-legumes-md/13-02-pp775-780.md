---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->

    
    
    
## でんぷん質、パスタ {#serie-des-farineux-et-pates-alimentaires}


### パルメザンチーズのフォンデュ {#fondus-au-parmesan}

### ニョッキ {#gnoki}

### カシャ {#kache}

### ポレンタ {#polenta}

### ラザーニェ[^1] {#lasagnes}

[^1]: 日本では「ラザニア」の表記のほうが一般的だが、ここではイタリア語での複数形
    にならってカナ書きした。フランス語ではイタリア語の lasagne の表記がそれ自体
    で複数形なのにもかかわらず、通常はフランス語の複数形を表わす s を付加して表
    記する。これらイタリア由来のパスタのフランス語表記には単数形、複数形をめぐっ
    て表記の混乱がしばしば見られるのも興味深い。
    
### マカロニ {#macaroni}

### ヌイユ {#nouilles}

### ラヴィオリ {#raviolis}

