---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->



## 3. ヴァノーとプリュヴィエの卵 {#oeufs-de-vanneau-et-de-pluvier}

羽毛は異なるが、ヴァノーとプリュヴィエは習性も生息場所も同じであり、卵
も似ている。

その卵は鳩の卵とほぼ同じ大きさで、殻は薄い緑色で黒い斑点がある。加熱す
ると、アルブミンを含んだ部分は乳白色になるが、他の卵のように白身がしっ
かり固まることはない。

普通の卵の調理方法はどれでも適用できるが、どちらかといえば固ゆで卵か冷
製で供する。

固ゆで卵にするには、湯に入れて再沸騰してから8分間加熱する。

調理する前に、冷水の入った鍋に入れて鮮度を確認しなければならない。浮い
てくるものはおしなべて鮮度が疑わしいので、よけておかなければならない。



<div class="recette">


#### ヴァノーの卵・アスピック仕立て {#oeufs-de-vanneau-en-aspic}

\index{oeuf@œuf!vanneau aspic@---s de Vanneau en Aspic}
\index{vanneau@vanneau!oeuf aspic@œufs de --- en Aspic}
\index{aspic@aspic!oeufs vanneau@œufs de Vanneau en ---}
\index{うあの@ヴァノー!たまこ あすひつく@---の卵・アスピック仕立て}
\index{たまこ@卵!うあの あすひつく@ヴァノーの---・アスピック仕立て}
\index{あすひつく@アスピック!うあの たまこ@ヴァノーの卵・---仕立て}

<div class="frsubenv">Œufs de Vanneau en Aspic</div>

（冷製）……縁に飾りのついた型の内側にジュレで膜を作り、好みの装飾をほ
どこす。ジュレを数滴たらして装飾のパーツを固定してから大さじ数杯のジュ
レをかぶせ、固まるまで置いておく。このジュレの上に固ゆでにして殻をむい
たヴァノーの卵を並べる。型からはずした後で立った状態になるように、先端
を下にすること。

ジュレの層を何度かくりかえして注ぎ、型を満たして仕上げる。提供直前に型
からはずしてナフキンの上に盛りつける。





\atoaki{}


#### ヴァノーの卵・クリスティアナ {#oeufs-de-vanneau-christiana}

\index{oeuf@œuf!vanneau christiana@---s de Vanneau Christiana}
\index{vanneau@vanneau!oeufs christiana@Œufs de --- Christiana}
\index{christiana@Christiana!oeufs vanneau@œufs de Vanneau ---}
\index{うあの@ヴァノー!たまこ くりすていあな@---の卵・クリスティアナ}
\index{たまこ@卵!うあの くりすていあな@ヴァノーの---・クリスティアナ}
\index{くりすていあな@クリスティアナ!うあの たまこ@ヴァノーの卵・---}

<div class="frsubenv">Œufs de Vanneau Christiana</div>

（温製）……[温製オードヴル](#oeufs-de-vanneau-christiana-chauds)参照。





\atoaki{}


#### ヴァノーの卵・デンマーク風 {#oeufs-de-vanneau-a-la-danoise}

\index{oeuf@œuf!vanneau danoise@---s de Vanneau à la Danoise}
\index{vanneau@vanneau!oeufs danoise!Œufs de --- à la Danoise}
\index{danois@danois(e)!oeufs vanneau@Œufs de Vanneau à la ---e}
\index{うあの@ヴァノー!たまこ てんまくふう@---の卵・デンマーク風}
\index{たまこ@卵!うあの てんまくふう@ヴァノーの---・デンマーク風}
\index{てんまく@デンマーク!うあの たまこ@ヴァノーの卵・---風}

<div class="frsubenv">Œufs de Vanneau à la Danoise</div>

（温製）……卵をポシェして、サーモンのピュレを詰めた小さなタルトレット
生地に盛りつける。





\atoaki{}


#### ヴァノーの卵・ガブリエル {#oeufs-de-vanneau-gabrielle}

\index{oeuf@œuf!gabrielle@---s de Vanneau Gabrielle}
\index{vanneau@vanneau!oeufs gabrielle@Œufs de --- Gabrielle}
\index{gabrielle@Gabrielle!oeufs vanneau@œufs de Vanneau ---}
\index{うあの@ヴァノー!たまこ かふりえる@---の卵・ガブリエル}
\index{たまこ@卵!うあの かふりえる@ヴァノーの---・ガブリエル}
\index{かふりえる@ガブリエル!うあの たまこ@ヴァノーの卵・---}

<div class="frsubenv">Œufs de Vanneau Gabrielle</div>

（冷製）……ダリオル型の内側にジュレで薄い膜を作り、オマールかラングス
トの卵をほぐして散らす。半熟にゆでて冷ましたヴァノーの卵を、先端を下に
して型に1個ずつ入れる。ジュレを満たして仕上げ、固まるまで置いておく。

卵を型からはずしてタルトレット生地の上に盛りつける。下部の周囲を取り巻
くように、生クリームを加えたソールのフィレのピュレを溝のある小さな口金
をつけた絞り袋でリボン状に絞り出す。





\atoaki{}


#### ヴァノーの卵・モデルヌ[^moderne] {#oeufs-de-vanneau-a-la-moderne}

\index{oeuf@œuf!vanneau moderne@---s de Vanneau à la Moderne}
\index{vanneau@vanneau!oeufs moderne@Œufs de Vanneau à la Moderne}
\index{moderne@moderne!oeufs vanneau@Œufs de Vanneau à la ---}
\index{うあの@ヴァノー!たまこ もてるぬ@---の卵・モデルヌ}
\index{たまこ@卵!うあの もてるぬ@ヴァノーの---・モデルヌ}
\index{もてるぬ@モデルヌ!うあの たまこ@ヴァノーの卵・---}

<div class="frsubenv">Œufs de Vanneau à la Moderne</div>

（冷製）……ダリオル型の内側にジュレで薄い膜を作り、シャルトルーズの装
飾を敷き込む。型に卵を1個ずつ、先端を下にして入れる。ジュレを満たして
仕上げ、固まるまで置いておく。

型からはずす。マヨネーズで[リエ\*](#lier-gls)した野菜のマセドワーヌをドーム型に
皿に盛り、周囲に環状に並べる。

[^moderne]: 近代、現代の、モダンな。




\atoaki{}


#### ヴァノーの卵・モスクワ風 {#oeufs-de-vanneau-a-la-moscovite}

<div class="frsubenv">Œufs de Vanneau à la Moscovite</div>

\index{oeuf@œuf!vanneau moscovite@---s de Vanneau à la Moscovite}
\index{vanneau@vanneau!oeufs moscovite@Œufs de --- à la Moscovite}
\index{moscovite@moscovite!oeufs vanneau@Œufs de Vanneau à la ---}
\index{うあの@ヴァノー!たまこ もすくわふう@---の卵・モスクワ風}
\index{たまこ@卵!うあの もすくわふう@ヴァノーの---・モスクワ風}
\index{もすくわ@モスクワ!うあの たまこ@ヴァノーの卵・---風}

（冷製）……卵を固茹でにし、冷まして殻をむく。

キャヴィアを詰めたタルトレットに卵を1個ずつ盛りつける。





\atoaki{}


#### ヴァノーの卵・巣ごもり仕立て {#oeufs-de-vanneau-dans-un-nid}

\index{oeuf@œuf!nid@---s de Vanneau dans un Nid}
\index{vanneau@vanneau!oeufs nid@Œufs de --- dans un Nid}
\index{nid@nid!oeufs vanneau@Œufs de Vanneau dans un ---}
\index{うあの@ヴァノー!たまこ すこもり@---の卵・巣ごもり仕立て}
\index{たまこ@卵!うあの すこもりしたて@ヴァノーの---・巣ごもり仕立て}
\index{すこもりしたて@巣ごもり仕立て!うあの たまこ@ヴァノーの卵・---}

<div class="frsubenv">Œufs de Vanneau dans un Nid</div>

（冷製）……丸皿にモンペリエバターで鳥の巣を形づくり、外側に絞り袋で細
工をほどこす。卵を半熟にゆでて冷ます。殻をむき、巣の縁に盛りつける。中
央に白いジュレをあしらい、巣の周囲にクレソンアレノワを飾る。





\atoaki{}


#### ヴァノーの卵のオムレツ {#omelette-d-oeufs-de-vanneau}

\index{oeuf@œuf!omelette vanneau@Omelette d'---s de Vanneau}
\index{omelette@omelette!oeufs vanneau@---　d'Œufs de Vanneau}
\index{vanneau@vanneau!omelette oeufs@Omelette d'Œufs de ---}
\index{うあの@ヴァノー!たまこ おむれつ@---の卵のオムレツ}
\index{たまこ@卵!うあの おむれつ@ヴァノーの---のオムレツ}
\index{おむれつ@オムレツ!うあの たまこ@ヴァノーの卵の---}

<div class="frsubenv">Omelette d'Œufs de Vanneau</div>

他のオムレツと同様に作るが、しっかりとしたアパレイユにするためにヴァノーの卵 6
個あたり鶏卵 1 個を加える。通常のオムレツの調理方法はどれでも適用できる。




\atoaki{}


#### ヴァノーの卵・プティットレーヌ {#oeufs-de-vanneau-petite-reine}

\index{oeuf@œuf!vanneau petite-reine@---s de Vanneau Petite-Reine}
\index{vanneau@vanneau!oeufs petite reine@Œufs de --- Petite-Reine}
\index{petite reine@petite-reine!oeufs vanneau@Œufs de Vanneau ---}
\index{うあの@ヴァノー!たまこ ふていつとれぬ@---の卵・プティットレーヌ}
\index{たまこ@卵!うあの ふていつとれぬ@ヴァノーの---・プティットレーヌ}
\index{ふていつとれぬ@プティットレーヌ!うあの たまこ@ヴァノーの卵・---}

<div class="frsubenv">Œufs de Vanneau Petite-Reine</div>

（冷製）……ダリオル型の内側に白いジュレで薄い膜を作る。ポーチドエッグ
の白身とトリュフをごく細かい市松模様に敷き詰める。中に固ゆでにした卵を
先端を下にして入れる。型にジュレを満たして仕上げ、固まるまで置いておく。

提供直前に型からはずす。環状に盛りつける。中央にアスパラガスの穂先のサ
ラダを付け合わせ、縁にぎざぎざのついた三日月形の白いジュレで皿を縁どる。





\atoaki{}


#### ヴァノーの卵・ロワイヤル {#oeufs-de-vanneau-a-la-royale}

\index{oeuf@œuf!vanneau royale@---s de Vanneau à la Royale}
\index{vanneau@vanneau!oeufs royale@Œufs de --- à la Royale}
\index{royal@royal(e)!oeufs vanneau@Œufs de Vanneau à la ---e}
\index{うあの@ヴァノー!たまこ ろわいやる@---の卵・ロワイヤル}
\index{たまこ@卵!うあの ろわいやる@ヴァノーの---・ロワイヤル}
\index{ろわいやる@ロワイヤル!うあの たまこ@ヴァノーの卵・---}

<div class="frsubenv">Œufs de Vanneau à la Royale</div>

（温製）……卵でつないでポシェした家禽のピュレのタルトレットを卵と同数
用意する。

型からはずす。円形に盛りつけ、タルトレットの中央を半分くらいくりぬいて、
半熟にゆでて殻をむいた卵を立てて盛りつけられるようにする。

卵にマッシュルームの軽いピュレを塗り、トリュフのみじん切りをふりかける。





\atoaki{}


#### ヴァノーの卵・トルバドゥール {#oeufs-de-vanneau-troubadour}

\index{oeuf@œuf!troubadour@---s de Vanneau Troubadour}
\index{vanneau@vanneau!oeufs troubadour@Œufs de --- à la Troubadour}
\index{troubadour@troubadour!oeufs vanneau@Œufs de Vanneau à la ---}
\index{うあの@ヴァノー!たまこ とるはとうる@---の卵・トルバドゥール}
\index{たまこ@卵!うあの とるはとうる@ヴァノーの---・トルバドゥール}
\index{とるはとうる@トルバドゥール!うあの たまこ@ヴァノーの卵・---}

<div class="frsubenv">Œufs de Vanneau à la Troubadour</div>

（温製）……大きなモリーユを卵と同じ数だけ選ぶ。モリーユの軸を取り除く。
開口部を開ける。味つけをしてバターでエチュヴェする。

ヴァノーの卵を半熟にゆで、殻をむく。

卵にエチュヴェしたモリーユを1個ずつかぶせる。フォワグラの軽いピュレを
詰めた溝のある小さなタルトレット生地に盛りつける。

卵を丸皿に環状に配する。





\atoaki{}


</div><!--endRecette-->
