---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->

<div class="main">

## 2. オムレツ

オムレツの調理に関するセオリーは、非常に単純であると同時に非常に複雑で
もある。なぜならば、オムレツについては人の好みがまちまちだからである。
よく焼いたものがいいという人もいれば、ほどよく火の通ったものでなければ
という人もいる。後者は結局のところ、とろとろ流れ出すようなオムレツでな
ければ評価しない。したがって、要はお客様の好みを把握しているということ
にある。

調理をおこなうには何よりもまず熟練と手先の器用さが問題となるので、そこ
に注意を向けることはしない。単に、卵の分子が均一になり、全体が柔らかく
仕上がるようにするべきだとだけ言っておこう。

つまるところ、オムレツとは何なのか？ 固まった卵で包みこんだ特殊なスク
ランブルエッグの一種、それ以外の何物でもない。

以下に続くレシピは、**卵3個分のオムレツ**を基本とする。味つけは粒の細
かい塩 $\frac{1}{2}$ つまみと胡椒ひとふり、調理にはバター15gを必要とす
る。

オムレツを巻いて皿の上にひっくり返したら、艶を出すために表面にバターを
ひとかけら塗るといい。任意だが、推奨すべき方法である。


</div><!--endMain-->
<div class="recette">

#### オムレツ・アニェス・ソレル {#omelette-agnes-sorel}

<div class="frsubenv">Omelette Agnès Sorel</div>


\index{omelette@omelette!agnes sorel@--- Agnès Sorel}
\index{agnes sorel@Agnès Sorel!omelette@omelette ---}
\index{おむれつ@オムレツ!あにえすそれる@---・アニェス・ソレル}
\index{あにえすそれる@アニェス・ソレル!おむれつ@オムレツ・---}

生のマッシュルーム30gを薄切りにしてバターでソテーし、家禽のピュレ大さ
じ1であえたものを包み込む。

オムレツの上にロングエカルラットの小さな輪切り8枚を重ね合わせるように
してのせ、周囲に仔牛のジュをリボン状に流す。





\atoaki{}


#### オムレツ・大公風 {#omelette-archiduc}

<div class="frsubenv">Omelette Archiduc</div>


\index{omelette@omelette!archiduc@--- Archiduc}
\index{archiduc@Archiduc!omelette@omelette ---}
\index{おむれつ@オムレツ!たいこうふう@---・大公風}
\index{たいこうふう@大公風!おむれつ@オムレツ・---}

大きな家禽のレバー2個を薄切りにしてバターで炒め、ソース・ドゥミグラス
であえたものを包み込む。オムレツの上に艶をつけたトリュフの薄片8枚をの
せ、周囲にソース・ドゥミグラスをリボン状に流す。





\atoaki{}


#### オムレツ・ベネディクティーヌ {#omelette-benedictine}

<div class="frsubenv">Omelette Bénédictine</div>


\index{omelette@omelette!benedictin@--- Bénédictine}
\index{benedictin@benedictin(e)!omelette@omelette ---}
\index{おむれつ@オムレツ!へねていくていぬ@---・ベネディクティーヌ}
\index{へねていくていぬ@ベネディクティーヌ!おむれつ@オムレツ・---}

温かい鱈のブランダードに $\frac{1}{4}$ 量のトリュフのみじん切りを加え
たものを包み込む。……オムレツの周囲にクリームソースをリボン状に流す。





\atoaki{}


#### オムレツ・ブシュリ[^1] {#omelette-a-la-bouchere}

<div class="frsubenv">Omelette à la Bouchère</div>


\index{omelette@omelette!boucher@--- à la Bouchère}
\index{boucher@boucher(ère)!omelette@omelette à la ---ère}
\index{おむれつ@オムレツ!ふしゆり@---・ブシュリ}
\index{ふしゆり@ブシュリ!おむれつ@オムレツ・---}

ごく新鮮な骨髄30gをさいの目に切ってポシェし、グラスドヴィアンドひとた
らしであえたものを包み込む。ポシェした骨髄の輪切り4枚をブロンド色のグ
ラスドヴィアンドでコーティングして、オムレツの上にのせる。

[^1]: 肉屋、の意。



\atoaki{}


#### オムレツ・ブローニュ風 {#omelette-a-la-boulonnaise}

<div class="frsubenv">Omelette à la Boulonnaise</div>


\index{omelette@omelette!boulonnais@--- à la Boulonnaise}
\index{boulonnais@boulonnais(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!ふろにゆふう@---・ブローニュ風}
\index{ふろにゆふう@ブローニュ風!おむれつ@オムレツ・---}

バターでソテーした大きな鯖の白子と、ノワゼット大のメートルドテルバター
を包み込む。……オムレツの周囲に溶かしたメートルドテルバター大さじ1を
流す。





\atoaki{}


#### オムレツ・ブルターニュ風 {#omelette-bretonne}

<div class="frsubenv">Omelette Bretonne</div>


\index{omelette@omelette!breton@--- Bretonne}
\index{breton@bereton(ne)!omelette@omelette ---ne}
\index{おむれつ@オムレツ!ふるたにゆふう@---・ブルターニュ風}
\index{ふるたにゆふう@ブルターニュ風!おむれつ@オムレツ・---}

卵を溶きほぐしたら、以下のものを加える。細かく刻んだ玉ねぎとポワローの
白い部分大さじ $\frac{1}{2}$ ずつをバターでエチュヴェしたもの、生のマッ
シュルームを薄切りにしてバターでエチュヴェしたもの大さじ。

通常どおりにオムレツを作る。





\atoaki{}


#### オムレツ・ブリヤサヴァラン {#omelette-brillat-savarin}

<div class="frsubenv">Omelette Brillat-Savarin</div>


\index{omelette@omelette!brillat-savarin@--- Brillat-Savarin}
\index{brillat-savarin@Brillat-Savarin!omelette@omelette ---}
\index{おむれつ@オムレツ!ふりやさうあらん@---・ブリヤサヴァラン}
\index{ふりやさうあらん@ブリヤサヴァラン!おむれつ@オムレツ・---}

さいの目に切ったベカスのフィレとトリュフ小さじ1ずつをベカスのクリであ
えたものを包み込む。オムレツの上に大きなトリュフの薄片3枚をのせ、周囲
にトリュフエッセンスで風味づけしたジビエのグラスをリボン状に流す。





\atoaki{}


#### オムレツ・ブリュッセル風 {#omelette-a-la-bruxelloise}

<div class="frsubenv">Omelette à la Bruxelloise</div>


\index{omelette@omelette!bruxellois@--- à la Bruxelloise}
\index{bruxellois@bruxellois(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!ふりゆつせるふう@---・ブリュッセル風}
\index{ふりゆつせるふう@ブリュッセル風!おむれつ@オムレツ・---}

ブレゼして細かく刻み、生クリームであえたアンディーヴ50gを包み込む。オ
ムレツの周囲にクリームソースをリボン状に流す。





\atoaki{}


#### オムレツ・シャルトル風（あるいはエストラゴン風味） {#omelette-a-la-chartres}

<div class="frsubenv">Omelette à la Chartres</div>


\index{omelette@omelette!chartres@--- à la Chartres}
\index{chartres@bchartres!omelette@omelette à la ---}
\index{おむれつ@オムレツ!しやるとるふう@---・シャルトル風}
\index{しやるとるふう@シャルトル風!おむれつ@オムレツ・---}

卵にエストラゴンのみじん切りをひとつまみ加える。通常どおりにオムレツを
作り、オムレツの中央に湯がいたエストラゴンの葉を椰子の葉模様に飾る。





\atoaki{}


#### オムレツ・シャスール {#omelette-chasseur}

<div class="frsubenv">Omelette Chasseur</div>


\index{omelette@omelette!chasseur@--- Chasseur}
\index{chasseur@chasseur!omelette@omelette ---}
\index{おむれつ@オムレツ!しやする@---・シャスール}
\index{しやする@シャスール!おむれつ@オムレツ・---}

大きな家禽のレバー2個のソテー・シャスールを包み込む。オムレツの真ん中
を縦に割る。割ったところに同じガルニチュールをブーケ状に飾り、周囲にソー
ス・シャスールをリボン状に流す。





\atoaki{}


#### オムレツ・シャトレーヌ {#omelette-chatelaine}

<div class="frsubenv">Omelette Châtelaine</div>


\index{omelette@omelette!chatelaine@--- Châtelaine}
\index{chatelaine@châtelaine!omelette@omelette ---}
\index{おむれつ@オムレツ!しやとれぬ@---・シャトレーヌ}
\index{しやとれぬ@シャトレーヌ!おむれつ@オムレツ・---}

栗40gをコンソメで煮て粗く刻み、グラスドヴィアンドひとたらしであえたも
のを包み込む。オムレツの周囲にスビーズを加えた家禽のヴルテをリボン状に
流す。





\atoaki{}


#### マッシュルーム入りオムレツ {#omelette-aux-champignons}

<div class="frsubenv">Omelette aux Champignons</div>


\index{omelette@omelette!champignon@--- aux Champignons}
\index{champignon@champignon!omelette@omelette aux ---s}
\index{おむれつ@オムレツ!まつしゆるむ@マッシュルーム入り---}
\index{まつしゆるむ@マッシュルーム!おむれつ@---入りオムレツ}

生のマッシュルーム40gを薄切りにしてバターでソテーしたものを卵に加える。
同様にソテーした大きなマッシュルームの薄切り6枚をオムレツの上にのせる。





\atoaki{}


#### オムレツ・ショワジー {#omelette-choisy}

<div class="frsubenv">Omelette Choisy</div>


\index{omelette@omelette!choisy@--- Choisy}
\index{choisy@Choisy!omelette@omelette ---}
\index{おむれつ@オムレツ!しよわし@---・ショワジー}
\index{しよわし@ショワジー!おむれつ@オムレツ・---}

ブレゼして細かく刻み、クリームソースであえたレチュ小さじ2を包み込
む。……オムレツの周囲に同じソースをリボン状に流す。





\atoaki{}


#### オムレツ・クラマール {#omelette-clamart}

<div class="frsubenv">Omelette Clamart</div>


\index{omelette@omelette!clamart@--- Clamart}
\index{clamart@Clamart!omelette@omelette ---}
\index{おむれつ@オムレツ!くらまる@---・クラマール}
\index{くらまる@クラマール!おむれつ@オムレツ・---}

生のプチポワ大さじ2に、火を通して細かく刻み、バターであえたレチュを加
えたものを包み込む。

オムレツを割り、中央に同じプチポワ大さじ1をあしらう。





\atoaki{}


#### オムレツ・クレシー {#omelette-crecy}

<div class="frsubenv">Omelette Crécy</div>


\index{omelette@omelette!crecy@--- Crécy}
\index{crecy@Crécy!omelette@omelette ---}
\index{おむれつ@オムレツ!くれし@---・クレシー}
\index{くれし@クレシー!おむれつ@オムレツ・---}

にんじんの赤い部分を薄切りにして、砂糖ひとつまみを加えてバターで火を通
し、こし器でこしたもの小さじ2を包み込む。オムレツの上にコンソメで煮て
グラセしたにんじんの薄切り4枚をのせる。周囲にクリームソースを流す。





\atoaki{}


#### クルヴェット入りオムレツ {#omelette-aux-crevettes}

<div class="frsubenv">Omelette aux Crevettes</div>


\index{omelette@omelette!crevette@--- aux Crevettes}
\index{crevette@crevette!omelette@omelette aux ---s}
\index{おむれつ@オムレツ!くるうえつと@クルヴェット入り---}
\index{くるうえつと@クルヴェット!おむれつ@---入りオムレツ}

クルヴェットの尾の身25gをソース・クルヴェット大さじ1であえたものを包み
込む。通常どおりにオムレツを割り、その真ん中にバターで火を通したクル
ヴェットの尾の身をブーケ状にあしらう。オムレツの周囲にソース・クルヴェッ
トをリボン状に流す。





\atoaki{}


#### オムレツ・デュラン {#omelette-durand}

<div class="frsubenv">Omelette Durand</div>


\index{omelette@omelette!durand@--- Durand}
\index{durand@Durand!omelette@omelette ---}
\index{おむれつ@オムレツ!てゆらん@---・デュラン}
\index{てゆらん@デュラン!おむれつ@オムレツ・---}

卵にマッシュルームとアーティチョークの花托15gずつを生で薄切りにし、バ
ターでソテーしたものを加える。

オムレツにトリュフのジュリエンヌ小さじ1と、同量のアスパラガスの穂先を
ヴルテ大さじ1であえたものを包み込む。周囲にトマト入りソース・ドゥミグ
ラスをリボン状に流す。





\atoaki{}


#### オムレツ・スペイン風 {#omelette-a-l-espagnole}

<div class="frsubenv">Omelette à l'Espagnole</div>


\index{omelette@omelette!espagnol@--- à l'Espagnole}
\index{espagnol@espagnol(e)!omelette@omelette à l'---e}
\index{おむれつ@オムレツ!すへいんふう@---・スペイン風}
\index{すへいんふう@スペイン風!おむれつ@オムレツ・---}

卵に以下のものを加える。ごく薄く切ってバターで火を通した玉ねぎ、トマト
フォンデュとパセリのみじん切りをその倍量。

オムレツをクレープの形に作る。





\atoaki{}


#### オムレツ・農家風 {#omelette-a-la-fermiere}

<div class="frsubenv">Omelette à la Fermière</div>


\index{omelette@omelette!fermier@--- à la Fermière}
\index{fermier@fermier(ère)!omelette@omelette à la ---ère}
\index{おむれつ@オムレツ!のうかふう@---・農家風}
\index{のうかふう@農家風!おむれつ@オムレツ・---}

卵に赤身のハムのみじん切り100gを加える。ひっくり返さずにごくごく柔らか
い状態を保ち、平たいオムレツに仕立てる。皿に移してパセリのみじん切りひ
とつまみをふりかける。




\atoaki{}


#### フィーヌゼルブ入りオムレツ {#omelette-aux-fines-herbes}

<div class="frsubenv">Omelette aux Fines Herbes</div>


\index{omelette@omelette!fines herbes@--- aux Fines Herbes}
\index{fines herbes@fines herbes!omelette@omelette aux ---}
\index{おむれつ@オムレツ!ふいぬせるふ@フィーヌゼルブ入り---}
\index{ふいぬせるふ@フィーヌゼルブ!おむれつ@---入りオムレツ}

卵に加えるフィーヌゼルブを構成するものは以下のとおり。パセリ、シブレッ
ト、セルフイユ、エストラゴンをみじん切りにしたもの。割合は大さじ1。

パセリだけで香りをつけたものをフィーヌゼルブ入りオムレツとするのは、完
全に間違っている。





\atoaki{}


#### かぼちゃの花入りオムレツ {#omelette-aux-fleurs-de-courge}

<div class="frsubenv">Omelette aux Fleurs de Courge</div>


\index{omelette@omelette!fleurs de courge@--- aux Fleurs de Courge}
\index{fleurs de courge@fleurs de courge!omelette@omelette aux ---}
\index{おむれつ@オムレツ!かほちやのはな@かぼちゃの花入り---}
\index{かほちやのはな@かぼちゃの花!おむれつ@---入りオムレツ}

卵にかぼちゃの花の花弁15gを加える。花は摘みたてのものを使い、細かく刻
んでバターで火を通し、パセリのみじん切りをひとつまみ加えておく。オムレ
ツはバターで焼いても油で焼いてもいいが、必ず周囲にトマトソースをリボン
状に流すこと。





\atoaki{}


#### オムレツ・フィレンツェ風 {#omelette-a-la-florentine}

<div class="frsubenv">Omelette à la Florentine</div>


\index{omelette@omelette!florentin@--- à la Florentine}
\index{florentin@florentin(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!ふいれんつえふう@---・フィレンツェ風}
\index{ふいれんつえふう@フィレンツェ風!おむれつ@オムレツ・---}

ゆでてエチュヴェしたほうれんそうの葉小さじ2を包み込む。オムレツの周囲
にソース・ベシャメルをリボン状に流す。





\atoaki{}



#### アーティチョークの花托入りオムレツ {#omelette-aux-fonds-d-artichauts}

<div class="frsubenv">Omelette aux Fonds d'Artichauts</div>


\index{omelette@omelette!fonds d'artichauts@--- aux Fonds d'Artichauts}
\index{fonds d'artichauts@fonds d'artichauts!omelette@omelette aux ---}
\index{おむれつ@オムレツ!あていちよくのかたく@アーティチョークの花托入り---}
\index{あていちよく@アーティチョーク!かたくおむれつ@---の花托入りオムレツ}

アーティチョークの花托50gを卵に加える。アーティチョークは生で薄切りに
してバターでソテーしておく。厚めに切ったものを5枚、一緒にソテーして取
り置いておき、オムレツの上にのせる。オムレツの周囲にとろみをつけたジュ
をリボン状に流す。





\atoaki{}


#### オムレツ・フォレスティエール {#omelette-a-la-forestiere}

<div class="frsubenv">Omelette à la Forestière</div>


\index{omelette@omelette!forestier@--- à la Forestière}
\index{forestier@forestier(ère)!omelette@omelette à la ---ère}
\index{おむれつ@オムレツ!ふおれすていえる@---・フォレスティエール}
\index{ふおれすていえる@フォレスティエール!おむれつ@オムレツ・---}

モリーユ50gをバターでソテーする。いちばん大きいもの3個を2つに切り、残
りは薄切りにする。

薄切りにしたモリーユをグラスドヴィアンドひとたらしであえる。

さいの目に切って下ゆでした塩漬け豚ばら肉40gをフライパンでソテーする。
卵をフライパンに注ぐ。オムレツを作り、あえたモリーユにパセリのみじん切
りひとつまみを加えたものを包み込む。取っておいた半割りのモリーユ6個を
オムレツの上に一列に並べ、周囲にとろみをつけたジュをリボン状に流す。





\atoaki{}


#### オムレツ・グロンメール {#omelette-grand-mere}

<div class="frsubenv">Omelette Grand'mère</div>


\index{omelette@omelette!grandmere@--- Grand'mère}
\index{grandmere@grand'mere!omelette@omelette ---}
\index{おむれつ@オムレツ!くろんめる@---・グロンメール}
\index{くろんめる@グロンメール!おむれつ@オムレツ・---}

溶きほぐしてパセリのみじん切りひとつまみを入れた卵に、パンの身で作った
クルトンを加える。クルトンは小さなさいの目に切ってバターでソテーし、熱々
のうちに卵に加えること。加えたら、ただちにオムレツを作る。





\atoaki{}


#### オムレツ・グランヴァル {#omelette-grandval}

<div class="frsubenv">Omelette Grandval</div>


\index{omelette@omelette!grandval@--- Grandval}
\index{grandval@Grandval!omelette@omelette ---}
\index{おむれつ@オムレツ!くらんうある@---・グランヴァル}
\index{くらんうある@グランヴァル!おむれつ@オムレツ・---}

軽くにんにくの香りをつけた濃いトマトフォンデュ大さじ2をオムレツに包み
込む。バターを加えたきめの細かいトマトソースを塗り、オムレツの上に固ゆ
で卵を温かいうちに輪切りにして並べる。ゆで卵の輪切りは大きい順に並べる
こと。





\atoaki{}


#### オムレツ・ハンガリー風 {#omelette-hongroise}

<div class="frsubenv">Omelette Hongroise</div>


\index{omelette@omelette!hongrois@--- Hongroise}
\index{hongrois@hongrois(e)!omelette@omelette ---e}
\index{おむれつ@オムレツ!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おむれつ@オムレツ・---}

細かく刻んでバターで火を通し、パプリカをきかせた玉ねぎ大さじ1に、皮を
むいて種をしぼり出し、粗く刻んで味付けし、バターでソテーしたトマト大さ
じ1強を加えたものを包み込む。

オムレツの周囲にハンガリー風ソースをリボン状に流す。





\atoaki{}


#### オムレツ・ジュラ風 {#omelette-jurassienne}

<div class="frsubenv">Omelette Jurassienne</div>


\index{omelette@omelette!jurassien@--- Jurassienne}
\index{jurassien@jurassien(ne)!omelette@omelette ---ne}
\index{おむれつ@オムレツ!しゆらふう@---・ジュラ風}
\index{しゆらふう@ジュラ風!おむれつ@オムレツ・---}

卵にシブレットとセルフイユのみじん切りをひとつまみずつ加える。前もって
フライパンで小さなラルドン25gを軽く炒めておき、そこに卵を注ぐ。ラルド
ンから出た脂でオムレツを焼く。巻く時に、細かく刻んでバターで柔らかく火
を通したオゼイユ大さじ1を包み込む。





\atoaki{}


#### オムレツ・ロレーヌ風 {#omelette-lorraine}

<div class="frsubenv">Omelette Lorraine</div>


\index{omelette@omelette!lorrain@--- Lorraine}
\index{lorrain@lorrain(e)!omelette@omelette ---e}
\index{おむれつ@オムレツ!ろれぬふう@---・ロレーヌ風}
\index{ろれぬふう@ロレーヌ風!おむれつ@オムレツ・---}

卵に以下のものを加える。塩漬け豚ばら肉30gを下ゆでしてグリルし、6個の小
さな長方形に分割したもの。ごく薄く削った若いグリュイエールチーズ25g。
シブレットのみじん切りひとつまみと、クレームエペス大さじ1。通常どおり
にオムレツを作る。





\atoaki{}


#### オムレツ・リヨン風 {#omelette-lyonnaise}

<div class="frsubenv">Omelette Lyonnaise</div>


\index{omelette@omelette!lyonnais@--- Lyonnaise}
\index{lyonnais@lyonnais(e)!omelette@omelette ---e}
\index{おむれつ@オムレツ!りよんふう@---・リヨン風}
\index{りよんふう@リヨン風!おむれつ@オムレツ・---}

玉ねぎ $\frac{1}{2}$ 個をごく薄く切り、フライパンで軽く色づくまでバター
で火を通す。パセリのみじん切りたっぷりひとつまみを入れた卵を加えて、通
常どおりにオムレツを作る。





\atoaki{}



#### オムレツ・マンセル {#omelette-mancelle}

<div class="frsubenv">Omelette Mancelle</div>


\index{omelette@omelette!mancelle@--- Mancelle}
\index{mancelle@Mancelle!omelette@omelette ---}
\index{おむれつ@オムレツ!まんせる@---・マンセル}
\index{まんせる@マンセル!おむれつ@オムレツ・---}

コンソメで煮て砕いた栗大さじ1と、ペルドローのフィレを短いジュリエンヌ
に切ってジビエのグラス大さじ1であえたもの大さじ1を包み込む。

オムレツの縦方向にジビエのグラスで細い筋を描き、周囲にジビエのフュメで
風味づけしたソース・ドゥミグラスをリボン状に流す。





\atoaki{}



#### オムレツ・マリージャンヌ {#omelette-marie-jeanne}

<div class="frsubenv">Omelette Marie-Jeanne</div>


\index{omelette@omelette!marie-jeanne@--- Marie-Jeanne}
\index{marie-jeanne@Marie-Jeanne!omelette@omelette ---}
\index{おむれつ@オムレツ!まりしやんぬ@---・マリージャンヌ}
\index{まりしやんぬ@マリージャンヌ!おむれつ@オムレツ・---}

少し脂身のある塩漬け豚ばら肉20gと、やや大きめのブリュノワーズに切った
じゃがいも30gをフライパンでソテーする。

卵を溶きほぐし、クレームエペス大さじ1強、オゼイユと若いほうれんそうを
バターでエチュヴェしたもの大さじ1、セルフイユのみじん切り小さじ1を加え
る。通常どおりにオムレツを作る。皿に盛ったら、溶かしバターをかけ、表面
に刻みたてのパセリのみじん切りを散らす。





\atoaki{}


#### オムレツ・マセナ {#omelette-massena}

<div class="frsubenv">Omelette Masséna</div>


\index{omelette@omelette!massena@--- Masséna}
\index{massena@Masséna!omelette@omelette ---}
\index{おむれつ@オムレツ!ませな@---・マセナ}
\index{ませな@マセナ!おむれつ@オムレツ・---}

アーティチョークの花托25gを薄切りにしてバターでソテーし、トマトソース
大さじ1であえたものを包み込む。オムレツの上にポシェした骨髄の大きな薄
切りにグラスドヴィアンドを塗ったものを2枚のせ、周囲にソース・ベアルネー
ズをリボン状に流す。





\atoaki{}


#### オムレツ・マクシム {#omelette-maxim}

<div class="frsubenv">Omelette Maxim</div>


\index{omelette@omelette!maxim@--- Maxim}
\index{maxim@Maxim!omelette@omelette ---}
\index{おむれつ@オムレツ!まくしむ@---・マクシム}
\index{まくしむ@マクシム!おむれつ@オムレツ・---}

通常どおりにオムレツを作り、上にエクルヴィスの尾の身とトリュフの薄片を
交互に一列に並べる。つづいて、かえるのもも肉のムニエルで周囲をきれいに
縁どる。





\atoaki{}


#### オムレツ・メキシコ風 {#omelette-mexicaine}

<div class="frsubenv">Omelette Mexicaine</div>


\index{omelette@omelette!mexicain@--- Mexicaine}
\index{mexicain@mexicain(e)!omelette@omelette ---e}
\index{おむれつ@オムレツ!めきしこふう@---・メキシコ風}
\index{めきしこふう@メキシコ風!おむれつ@オムレツ・---}

卵に生のマッシュルーム25gをスライスしてバターでソテーしたものと赤ピー
マンのみじん切り10gを加える。オムレツを巻く際に、濃いトマトフォンデュ
大さじ1を包み込む。





\atoaki{}


#### オムレツ・ミレイユ {#omelette-mireille}

<div class="frsubenv">Omelette Mireille</div>


\index{omelette@omelette!mireille@--- Mireille}
\index{mireille@Mireille!omelette@omelette ---}
\index{おむれつ@オムレツ!みれいゆ@---・ミレイユ}
\index{みれいゆ@ミレイユ!おむれつ@オムレツ・---}

少量の砕いたにんにくで風味づけしたトマトフォンデュ大さじ1強を包み込む。
オムレツは油で焼き、周囲にサフランで色と香りをつけたクリームソースをリ
ボン状に流す。





\atoaki{}


#### オムレツ・モンスレ {#omelette-monselet}

<div class="frsubenv">Omelette Monselet</div>


\index{omelette@omelette!monselet@--- Monselet}
\index{monselet@Monselet!omelette@omelette ---}
\index{おむれつ@オムレツ!もんすれ@---・モンスレ}
\index{もんすれ@モンスレ!おむれつ@オムレツ・---}

フォワグラの軽いピュレ大さじ1に、短いジュリエンヌに切ったマッシュルー
ムとトリュフを大さじ $\frac{1}{2}$ ずつと、アスパラガスの穂先小さじ1を
加えたものを包み込む。

オムレツの上に艶をつけた大きなトリュフの薄片を飾り、周囲にソース・ドゥ
ミグラスをリボン状に流す。





\atoaki{}


#### オムレツ・ムスリーヌ {#omelette-mousseline}

<div class="frsubenv">Omelette Mousseline</div>


\index{omelette@omelette!mousseline@--- Mousseline}
\index{mousseline@mousseline!omelette@omelette ---}
\index{おむれつ@オムレツ!むすりぬ@---・ムスリーヌ}
\index{むすりぬ@ムスリーヌ!おむれつ@オムレツ・---}

卵の黄身3個分を鉢に入れ、塩 $\frac{1}{2}$ つまみを加えてごく濃い生クリー
ム大さじ1でのばす。

卵白3個分をごく固く泡立てて加える。大きなフライパンにバター30gを入れて
十分に熱し、このアパレイユを流し込む。フライパンを小刻みに、かつ非常に
すばやくあおって、オムレツの縁の部分を中央に持ってくるようにする。

アパレイユが均等に固まったらすぐにオムレツを巻き、皿の上に返して、ただ
ちに提供する。





\atoaki{}


#### オムレツ・ノンテュア {#omelette-nantua}

<div class="frsubenv">Omelette Nantua</div>


\index{omelette@omelette!nantua@--- Nantua}
\index{nantua@Nantua!omelette@omelette ---}
\index{おむれつ@オムレツ!のんてゆあ@---・ノンテュア}
\index{のんてゆあ@ノンテュア!おむれつ@オムレツ・---}

エクルヴィスの尾の身30gをソース・ノンテュア大さじ1であえたものを包み込
む。オムレツの上に大きなエクルヴィスの尾の身2本をのせて中央にトリュフ
の薄片を1枚飾り、周囲にソース・ノンテュアをリボン状に流す。





\atoaki{}


#### しらす入りオムレツ {#omelette-aux-nonnats}

<div class="frsubenv">Omelette aux Nonnats</div>


\index{omelette@omelette!nonnat@--- aux Nonnats}
\index{nonnat@nonnat!omelette@omelette aux ---s}
\index{おむれつ@オムレツ!しらす@しらす入り---}
\index{しらす@しらす!おむれつ@---入りオムレツ}

フライパンに流し込む直前に卵に澄ましバターでソテーしたしらす30gを加え、
通常どおりにオムレツを作る。





\atoaki{}


#### オムレツ・ノルマンディー風 {#omelette-a-la-normande}

<div class="frsubenv">Omelette à la Normande</div>


\index{omelette@omelette!normand@--- à la Normande}
\index{normand@normand(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!のるまんていふう@---・ノルマンディー風}
\index{のるまんていふう@ノルマンディー風!おむれつ@オムレツ・---}

ポシェしてひだを取り、ソース・ノルマンドであえた牡蠣6個を包み込む。オ
ムレツの周囲に同じソースをリボン状に流す。





\atoaki{}


#### オゼイユ入りオムレツ {#omelette-a-l-oseille}

<div class="frsubenv">Omelette à l'Oseille</div>


\index{omelette@omelette!oseille@--- à l'Oseille}
\index{oseille@oseille!omelette@omelette à l'---}
\index{おむれつ@オムレツ!おせいゆ@オゼイユ入り---}
\index{おせいゆ@オゼイユ!おむれつ@---入りオムレツ}

オムレツを巻く時に、刻んでバターで煮溶かし、セルフイユのみじん切りをひ
とつまみ加えたオゼイユ大さじ1強を包み込む。





\atoaki{}


#### オムレツ・パルマンティエ {#omelette-parmentier}

<div class="frsubenv">Omelette Parmentier</div>


\index{omelette@omelette!parmentier@--- Parmentier}
\index{parmentier@Parmentier!omelette@omelette ---}
\index{おむれつ@オムレツ!はるまんていえ@---・パルマンティエ}
\index{はるまんていえ@パルマンティエ!おむれつ@オムレツ・---}

卵にパセリのみじん切りをひとつまみ加え、卵をフライパンに流し入れる直前
に、小さなさいの目に切って味つけし、バターでソテーしたじゃがいも大さじ
2を熱々のうちに加える。通常どおりにオムレツを作る。





\atoaki{}


#### オムレツ・ペイザンヌ {#omelette-a-la-paysanne}

<div class="frsubenv">Omelette à la Paysanne</div>


\index{omelette@omelette!paysan@--- à la Paysanne}
\index{paysan@paysan(ne)!omelette@omelette à la ---ne}
\index{おむれつ@オムレツ!へいさんぬ@---・ペイザンヌ}
\index{へいさんぬ@ペイザンヌ!おむれつ@オムレツ・---}

小さなさいの目に切って下ゆでした塩漬け豚ばら肉50gにバターを使ってフラ
イパンで焼き色をつける。

卵に以下のものを加える。薄い輪切りにしてバターでソテーしたじゃがいも大
さじ1、刻んでバターで煮溶かしたオゼイユ大さじ $\frac{1}{2}$ 、粗く刻ん
だセルフイユひとつまみ。卵を豚ばら肉の上に流し込む。卵を柔らかさを保っ
た状態に焼く。オムレツをクレープのように返し、ただちに滑らせるようにし
て丸皿に移す。





\atoaki{}


#### アスパラガスの穂先入りオムレツ {#omelette-aux-pointes-d-asperges}

<div class="frsubenv">Omelette aux Pointes d'Asperges</div>


\index{omelette@omelette!asperge@--- aux Pointes d'Asperges}
\index{asperge@asperge!omelette@omelette aux Pointes d'---s}
\index{おむれつ@オムレツ!あすはらかす@アスパラガスの穂先入り---}
\index{あすはらかす@アスパラガス!おむれつ@---の穂先入りオムレツ}

ゆでてバターでエチュヴェしたアスパラガスの穂先大さじ1 $\frac{1}{2}$ を
卵に加える。オムレツを盛りつけたら真ん中を割り、上にアスパラガスの穂先
を形よく小さな束にしてのせる。





\atoaki{}


#### オムレツ・ポルトガル風 {#omelette-a-la-portugaise}

<div class="frsubenv">Omelette à la Portugaise</div>


\index{omelette@omelette!portugais@--- à la Portugaise}
\index{portugais@portugais(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!ほるとかるふう@---・ポルトガル風}
\index{ほるとかるふう@ポルトガル風!おむれつ@オムレツ・---}

トマトフォンデュ大さじ1強を包み込む。オムレツの周囲にバターを加えたト
マトソースをリボン状に流す。





\atoaki{}


#### 高位聖職者のオムレツ {#omelette-des-prelats}

<div class="frsubenv">Omelette des Prélats</div>


\index{omelette@omelette!prelat@--- des Prélats}
\index{prelat@prélat!omelette@omelette des ---s}
\index{おむれつ@オムレツ!こういせいしよくしや@高位聖職者の---}
\index{こういせいしよくしや@高位聖職者!おむれつ@---のオムレツ}

白子、エクルヴィスの尾の身、ごく短いジュリエンヌにしたトリュフの大きな
サルピコン大さじ1強をエクルヴィスバターを加えて仕上げたソース・ノルマ
ンド大さじ1であえたものを包み込む。

オムレツに同じソースを塗り、濃い黒色のトリュフのみじん切りをたっぷりふりかける。





\atoaki{}


#### オムレツ・プランセス {#omelette-princesse}

<div class="frsubenv">Omelette Princesser</div>


\index{omelette@omelette!princesse@--- Princesse}
\index{princesse@Princesse!omelette@omelette ---}
\index{おむれつ@オムレツ!ふらんせす@---・プランセス}
\index{ふらんせす@プランセス!おむれつ@オムレツ・---}

クリームソースであえたアスパラガスの穂先大さじ1を包み込む。オムレツの
上にトリュフの薄片を連ねてのせ、周囲にクリームソースをリボン状に流す。





\atoaki{}


#### オムレツ・プロヴァンス風 {#omelette-a-la-provencale}

<div class="frsubenv">Omelette à la Provençale</div>


\index{omelette@omelette!provencal@--- à la Provençale}
\index{provencal@provençal(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!ふろうあんすふう@---・プロヴァンス風}
\index{ふろうあんすふう@プロヴァンス風!おむれつ@オムレツ・---}

フライパンの底ににんにく1かけを軽くこすりつける。次に、油大さじ2を入れ
て煙が立つまで熱する。大きなトマト1個の皮をむいて、しぼって種を抜き、
大きなさいの目に切って、粗く刻んだパセリひとつまみを加えたものを投入す
る。……通常どおりにオムレツを作る。

###### 【原注】 {#nota-omelette-a-la-provencale}

この料理の性格からいって、トマトは油でソテーしなければならないが、やむ
を得ない場合はかわりに澄ましバターを用いてもよい。





\atoaki{}


#### オムレツ・レーヌ{#omelette-a-la-reine}

<div class="frsubenv">Omelette à la Reine</div>


\index{omelette@omelette!reine@--- à la Reine}
\index{reine@reine!omelette@omelette à la ---}
\index{おむれつ@オムレツ!れぬ@---・レーヌ}
\index{れぬ@レーヌ!おむれつ@オムレツ・---}

家禽のピュレ小さじ2を包み込む。オムレツの周囲にソース・シュプレームを
リボン状に流す。





\atoaki{}


#### ロニョン入りオムレツ {#omelette-aux-rognons}

<div class="frsubenv">Omelette aux Rognons</div>


\index{omelette@omelette!rognon@--- aux Rognons}
\index{rognon@rognon!omelette@omelette aux ---s}
\index{おむれつ@オムレツ!ろによん@ロニョン入り---}
\index{ろによん@ロニョン!おむれつ@---入りオムレツ}

仔牛のロニョンか羊のロニョンをさいの目に切って塩と胡椒で味つけし、バター
でしっかりソテーしてソース・ドゥミグラスであえたもの大さじ1
$\frac{1}{2}$ を包み込む。

オムレツを盛りつけたら、真ん中に割れ目を入れて同じガルニチュール大さじ
1をのせる。

周囲にソース・ドゥミグラスをリボン状に流す。





\atoaki{}


#### オムレツ・ロッシーニ {#omelette-rossini}

<div class="frsubenv">Omelette Rossini</div>


\index{omelette@omelette!rossini@--- Rossini}
\index{rossini@Rossini!omelette@omelette ---}
\index{おむれつ@オムレツ!ろつしに@---・ロッシーニ}
\index{ろつしーに@ロッシニ!おむれつ@オムレツ・---}

味つけをして溶きほぐした卵に、焼いたフォワグラとトリュフをそれぞれ小さ
なさいの目に切って大さじ1ずつ加える。

オムレツの上に小さな長方形に切ったフォワグラを温めてのせ、その両端にト
リュフの薄片を1枚ずつ飾る。オムレツの縦方向に配置すること。

周囲にトリュフエッセンスを加えたソース・ドゥミグラスをリボン状に流す。





\atoaki{}


#### オムレツ・ルーアン風 {#omelette-a-la-rouennaise}

<div class="frsubenv">Omelette à la Rouennaise</div>


\index{omelette@omelette!rouennais@--- à la Rouennaise}
\index{rouennais@rouennais(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!るあんふう@---・ルーアン風}
\index{るあんふう@ルーアン風!おむれつ@オムレツ・---}

鴨のフォワグラをエシャロットとともにバターでソテーしてすりつぶし、裏ご
しして作ったピュレ大さじ1強を包み込む。

オムレツの周囲にルーアン風ソースをリボン状に流す。あるいは、シンプルに
上等の赤ワインを煮詰めてグラスドヴィアンドを加え、バターで軽くモンテし
たものを用いる。





\atoaki{}


#### オムレツ・サヴォワ風 {#omelette-a-la-savoyarde}

<div class="frsubenv">Omelette à la Savoyarde</div>


\index{omelette@omelette!savoyard@--- à la Savoyarde}
\index{savoyard@savoyard(e)!omelette@omelette à la ---e}
\index{おむれつ@オムレツ!さうおわふう@---・サヴォワ風}
\index{さうおわふう@サヴォワ風!おむれつ@オムレツ・---}

クレームエペス大さじ2、生で薄い輪切りにしてバターでソテーしたじゃがい
も25g、ごく薄く削った若いグリュイエールチーズ20gを卵に加える。……オム
レツをクレープの形に作る。





\atoaki{}


#### オムレツ・スイス風 {#omelette-a-la-suissesse}

<div class="frsubenv">Omelette à la Suissesse</div>


\index{omelette@omelette!suisse@--- à la Suissesse}
\index{suisse@suisse(sse)!omelette@omelette à la ---sse}
\index{おむれつ@オムレツ!すいすふう@---・スイス風}
\index{すいす@スイス!おむれつ@オムレツ・---風}

エメンタールチーズ50gと生クリーム大さじ1を卵に加え、オムレツをクレープ
の形に作る。





\atoaki{}


#### ツナ入りオムレツ {#omelette-au-thon}

<div class="frsubenv">Omelette au Thon</div>


\index{omelette@omelette!thon@--- aux Thon}
\index{thon@thon!omelette@omelette aux ---}
\index{おむれつ@オムレツ!つな@ツナ入り---}
\index{つな@ツナ!おむれつ@---入りオムレツ}

ツナの油漬け30gをさいの目に切って卵に加える。通常どおりにオムレツを作
り、溶かしたアンチョビバター大さじ2をかける。





\atoaki{}


#### トリュフ入りオムレツ {#omelette-aux-truffes}

<div class="frsubenv">Omelette au Truffes</div>


\index{omelette@omelette!truffe@--- aux Truffes}
\index{truffe@truffe!omelette@omelette aux ---s}
\index{おむれつ@オムレツ!とりゆふ@トリュフ入り---}
\index{とりゆふ@トリュフ!おむれつ@---入りオムレツ}

薄い輪切りにしたトリュフ大さじ1強を卵に加える。オムレツの上に艶をつけ
たトリュフの薄片を連ねてのせ、周囲にグラスドヴィアンドを細く流す。





\atoaki{}


#### オムレツ・ヴィクトリア {#omelette-victoria}

<div class="frsubenv">Omelette Victoria</div>


\index{omelette@omelette!victoria@--- Victoria}
\index{victoria@Victoria!omelette@omelette ---}
\index{おむれつ@オムレツ!ういくとりあ@---・ヴィクトリア}
\index{ういくとりあ@ヴィクトリア!おむれつ@オムレツ・---}

さいの目に切ったラングストの尾の身とトリュフをソース・オマールであえた
サルピコン大さじ1強を包み込む。オムレツの周囲に同じソースをリボン状に
流す。





\atoaki{}


</div><!--endRecette-->

