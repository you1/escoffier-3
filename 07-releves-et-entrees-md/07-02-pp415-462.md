---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->




## 肉料理

## I. 牛肉


### アロワイヨ

\index{aloyau@aloyau}
\index{あろわいよ@アロワイヨ}


アロワイヨという部位はハンギングテンダーの先端から最初のコット[^1]までで、フィレ
全体を含んだものをいう。この塊肉を「アロワイヨ」と呼べるのは、サーロインとフィレ
を取りはずさないままにカットされている場合だけだ。

だがアロワイヨ全体を丸ごと調理するには、バヴェット[^2]は外して短かくせ
ざるを得ない。それからサーロインにある肋骨に沿ってある筋のところで切り
わけ、他の部分も同様にする。フィレ肉の部分はやや脂をのこしておき、サー
ロインも完全に脂を切り捨てないこと。

通常の厨房業務で、アロワイヨをブレゼにする場合は、平均 3 kg に横に切り
わける。ローストの場合は全体をそのまま残すようにするのがいい。

アロワイヨをルルヴェとして仕立てる場合はブレゼかローストとし、ロースト
の場合、お客様が望むならセニャン[^3]に仕上げてそれ以上火が通らないよう
にすること。ブレゼの場合は、最上級の品質のものでなければ、具合の悪いこ
とに、ぱさついた仕上がりになることが多い。

牛フィレの料理に合わせられるガルニチュールはどれもアロワイヨに使える。
とはいえ一般的には、[ブルジョワーズ](#garniture-bourgeoise)、セロ
リ、[フランドル風](#garniture-flamande)、[リシュ
リュー](#garniture-richelieu)、[プロヴァンス
風](#garniture-provencale)などの大きなものから選ぶ。

ブレゼの煮汁はこうしたガルニチュールのレシピに合わせてソースに仕立てる。


[^1]: 肋骨

[^2]: いわゆるカイノミに相当する部位でバヴェットのエシャロット風味
    bavette à l'échalote（バヴェットアレシャロット）などに用いられる
    Bavette d'aloyau （バヴェットダロワイヨ）と、おもに煮込み料理に用
    いられる bavette de flanchet （バヴェットドゥフロンシェ）がある。
    いわゆる《《ハラミ》》（フランス語では onglet オングレ）とは異な
    る部位であり、オングレが内臓などとおなじ副生物として扱われているの
    に対し、バヴェットはいずれも正肉。
    
    
[^3]: 肉の火の通し加減。血の滴るような、の意。
    
### アロワイヨの冷製

アロワイヨを冷製として供する場合には、きれいに掃除をして形状を整え[^4]、
ジュレを塗って充分な厚さの層になるようにする。それからプラトーに盛り付
け、周囲をジュレで囲んでやる。プラトーの周囲には、正確にカットした大き
なさいの目のジュレ[^5]を飾る。

[^4]: 「ブレゼした後に」という表現が当然のこととして省かれていることに
    注意。


[^5]: 原文 gros croûtons de gelée （グロクルゥトンドゥジュレ）。このよ
    うに croûton の語はさいの目に切ったジュレについても用いられる。
    

### アムレットとセルヴェル {#amourettes-et-cervelles}

牛および仔牛の脊髄は\kenten{アムレット}と呼ばれる。たいていはセルヴェ
ル[^6]と一緒に売られていて、セルヴェルと一緒に料理することが多い。けれ
ども場合によっては、セルヴェルとは別に下ごしらえし、それぞれ別の料理や
ガルニチュールにすることがある。

食感が軽いため、セルヴェルとアムレットは幼児や老人の食事では最上級のも
のとされている。

繊細な調理をするには、セルヴェルとアムレットは成牛より仔牛のものが好ま
れる。けれど、料理そのものは本質的にはおなじものだ。


[^6]: 脳、の意。

### 下処理の基本 {#amourettes-et-cervelles-principe-de-traitement}

充分に流水にさらして血抜きをしたら、アムレットとセルヴェルはリモネ[^7]、
つまりは大脳皮質を覆っている神経線維（軟膜と硬膜）を取り除く。さらに冷
水にさらして、しっかりと血抜きをする。要するに付着した血液をすべて洗い
流すわけだ。

火入れには[クールブイヨンA](#court-bouillon-a)で示した比率のヴィネガー
を効かせたクールブイヨンを用いる。加熱時間は 25 〜 30 分。

クールブイヨンにはあらかじめ火を入れておいてもいいし、その場で火にかけ
てもいい。けれどもどんな場合でもクールブイヨンは漉してから使うこと。完
全に沸騰した状態になってからアムレットやセルヴェルを投入する。身質の締
まった仕上りにする方法はこれ以外にはない。

[^7]: limoner （リモネ）通常は淡水魚などの\kenten{ぬめり}を取り除くこ
    とだが、ここでは本文にあるとおり。

### アムレット

\index{amourette@amourette}
\index{あむれつと@アムレット}
\index{せきつい@脊髄|see {アムレット}}

<!--この最後の見出しが節の見出しになります-->
<!--
\index{amourettes|(}
\index{あむれつと|(}
\index{せきつい@脊髄|see {アムレット}}
-->

</div><!--endMain-->

<div class="recette"> <!--二段組スタート-->

#### アムレットのクロメスキ・フランセーズ {#cromesquis-d-amourettes-a-la-francaise}

\index{cromesquis@cromesquis!amourette francaise@--- d'Amourettes à la française}
\index{amourette@amourette!cromesquis francaise@Cromesquis d'--- à la française}
\index{francais@français(e)(à la)!cromesquis amourettes@Cromesquis d'Amourettes à la ---z}
\index{あむれつと@アムレット!くろめすきふらんせす@---のクロメスキ・アラフランセーズ}
\index{くろめすき@クロメスキ!あむれつとふらんせす@アムレットの---・アラフランセーズ}
\index{ふらんせす@アラフランセーズ!あむれつとくろめすき@アムレットのクロメスキ・---}
\index{ふらんすふう@フランス風|see{フランセーズ}} <!--この行は1回あればいいので見なかったことにしてください。-->

<div class="frsubenv">Cromesquis d'Amourettes à la française (1-4)</div>

<!--ここからレシピの訳を書いてください。段落代えは1行アキを入れてください。-->
クロケットの項で示した分量比率に従ってアムレットとマッシュルーム、トリュフのサルピコンを用意し、煮詰められたソース・アルマンドであえる。（オードブル・ショー参照）
75gずつに分割し、直方体に整形する。

提供する際に、軽い揚げ衣に潜らせ、熱した大きな揚げ物鍋で揚げる。ナプキンの上に揚げたパセリとともに盛り付ける。

別添えで、ソース・ペリグー、フィーヌゼルブ、または、準ずるものを添える。

\atoaki{}

#### アムレットのクロメスキ・ポーラーンド風 {#cromesquis-d-amourettes-a-la-polonaise}

\index{cromesquis@cromesquis!amourette polonaise@--- d'Amourettes à la polonaise}
\index{amourette@amourette!cromesquis polonaise@Cromesquis d'--- à la polonaise}
\index{polonais@polonais(e)(à la)!cromesquis amourettes@Cromesquis d'Amourettes à la ---z}
\index{あむれつと@アムレット!くろめすきほらんとふう@---のクロメスキ・ポロネーズ}
\index{くろめすき@クロメスキ!あむれつとほらんとふう!アムレットの---・アラポロネーズ}
\index{ほらんとふう@ポーランド風!あむれつとくろめすき@アムレットのクロメスキ・---}

<div class="frsubenv">Cromesquis d'Amourettes à la Polonaise (1-4)</div>

通常のクロケットの分量比率で、アムレットとセップ、トリュフのサルピコンを用意し、煮詰められたソース・エスパニョルであえる。
いくつかの直方体に整形する。

糖を加えずに薄めに焼き上げたクレープで一つずつ包み、軽い揚げ衣に潜らせ、熱した大き目の揚げ物鍋で揚げる。
ナプキンの上に束ねたパセリのフライとともに盛り付ける。

別添えで、ソース・フィーヌゼルブ、またはソース・デュクセルを添える。

\atoaki{}

#### アムレットのクロメスキ　ロシア風 {#cromesquis-d-amourettes-a-la-russe}

\index{cromesquis@cromesquis!amourette russe@--- d'Amourettes à la russe}
\index{amourette@amourette!cromesquis russe@Cromesquis d'--- à la russe}
\index{russe@russe!cromesquis amourettes@Cromesquis d'Amourettes à la ---}
\index{あむれつと@アムレット!くろめすきろしあふう@---のクロメスキ・ろしあふう}
\index{くろめすき@クロメスキ!あむれつとあらりゅす@アムレットの---・ろしあふう}
\index{ろしあふう@ロシア風!あむれつとくろめすき@アムレットのクロメスキ・---}

<div class="frsubenv">Cromesquis d'Amourettes à la russe (1-4)</div>
上記のようにソース・エスパニョルで和えたアパレイユを準備し、同じように直方体に整形する。

これらを、鮮度が良くキメの細かい豚の網脂で包む。ビールを加えた揚げ衣に潜らせ、熱した大き目の揚げ物鍋で
揚げる。

ナフキンの上に揚げたパセリと共に盛り付ける。上記を提供する際に同時に「クロメスキ・アラポロネーズ」で示したソースも提供する。



\atoaki{}


#### アムレットのクロメスキ[^9] {#cromesquis-d-amourettes}

\index{cromesquis@cromesquis!amourette@--- d'Amourettes}
\index{amourette@amourette!cromesquis@Cromesquis}
\index{あむれつと@アムレット!くろめすき@---のクロメスキ}
\index{くろめすき@クロメスキ!あむれつと@アムレットの---}

<div class="frsubenv">Cromesquis d'Amourettes (4)</div>

クロメスキ・アラフランセーズのものと同じアパレイユの製法をとる。

同じ重さに分割したクロメスキを、それぞれ小円盤型にし、イギリス式に小麦粉
をまぶし、卵を塗りパンの衣をまとわせる。

熱した大きな揚げ物鍋を用いて揚げ、ナフキンの上に揚げたパセリとともに盛り付ける。

別添えで、[ソース・ペリグー](#sauce-perigueux)、または仕上げにバターを
加えた[ソース・ドゥミグラス](#sauce-demi-glace)を添える。

[^9]: 初版〜第三版まで、この位置にアムレットの\kenten{クロケット} croquettes が収録されていた。内容はまったくおなじ。

\atoaki{}

#### アムレットのフリトー {#fritot-d-amourettes}

\index{Fritot@Fritot!amourette@--- d'Amourettes}
\index{amourette@amourette!fritot@Fritot d'---}
\index{あむれつと@アムレット!ふりと@---のフリトー}
\index{ふりと@フリトー!あむれつと@アムレットの---}

<div class="frsubenv">Fritot d'Amourettes (1-4)</div>

ポシェしたてのアムレットを7センチメートルの長さにトロンソンに切り、レモン汁、オイルひと垂らし、塩、胡椒、パセリの[アッシェ\*](#hacher-gls)とともに20分前にマリネしておく。

提供するタイミングで、軽い揚げ衣に潜らせて、熱した大きな揚げ物鍋を用いて揚げ、ナプキンの上に揚げたパセリとともに盛り付ける。

別添えでソース・トマトを添える。

\atoaki{}

#### アムレットのタンバル・スコットランド風{#timbales-d-amourettes-a-l-ecossaise}


\index{amourette@amourette!timbale ecossaise@Timbales d'--- à l'ecossaise}
\index{timbale@timbale!amourettes ecossaise@---s d'Amourettes à l'écossaise}
\index{ecossais@écossais(e)!timbale amourettes!Timbales d'Amourettes à l'---e}
\index{あむれつと@アムレット!たんはるすこつとらんと@---のタンバル・スコットランド風}
\index{たんはる@タンバル!あむれつとすこつとらんと@アムレットの---・スコットランド風}
\index{すこつとらんとふう@スコットランド風!あむれつとのたんはる@アムレットのタンバル・---}

<div class="frsubenv">Timbales d'Amourettes à l'écossaise (1-4)</div>

内側にバターを塗った六角形の型の底に赤い牛タンの塩漬けの薄切りを敷き込む。
型の内側全体に3〜4ミリメートルの厚さでファルスを張り付け、[ソース・アルマンド](#sauce-allemande)であえた、赤い牛タンの塩漬けとアムレットのサルピコンをその中に配置する。
[ファルス](#farces)で蓋をする。

12分間湯煎で[ポシェ\*](pocher-gls)する。丸い皿に、裏返して型から外しながら冠状に盛り付ける。
（出来上がりは、上部に牛タンの薄切りがのったファルスの中心にサルピコンが射込まれている六角形の料理が丸皿にリング状に盛り付けられている形となる。）

別添えで[スコットランド風ソース](#sauce-a-l-ecossaise)を添える

\atoaki{}



#### アムレットのタンバル・ナポリ風 {#timbales-d-amourettes-a-la-napolitaine}

\index{amourette@amourette!timbale napolitaine@Timbales d'--- à la napolitaine}
\index{timbale@timbale!amourettes napolitaine---s d'Amourettes à la Napolittaine}
\index{ecossais@écossais(e)!timbale amourettes!Timbales d'Amourettes à l'---e}
\index{あむれつと@アムレット!たんはるなほりふう@---のタンバル・ナポリ風}
\index{たんはる@タンバル!あむれつとなほりふう@アムレットの---・ナポリ風}
\index{なほりふう@ナポリ風!あむれつとのたんはる@アムレットのタンバル・---}

<div class="frsubenv">Timbales d'Amour à la napolitaine (1-4)</div>

たっぷりとバターを塗ったダリオル型に太いマカロニを敷き詰める。
マカロニはやや固めにポシェし、よく水気を切って布で水分を取り除き、らせん状に立ててやる。
（貼り付けたパスタを）内側からファルスの薄い層でつなぎ止めてやる。
ソース・ドゥミグラスで和えた、アムレットとマッシュルームのサルピコンをその内側に配置するが、この際のソース・ドゥミグラスはトマトを効かてよく煮詰めておくこと。
ファルスで蓋をするように覆い、ポシェする。前述のスコットランド風タンバルのように盛り付ける。

別添えで、トマトをよく効かせ、軽くバターの香りを付けたソース・ドゥミグラスを添える。

\atoaki{}


#### アムレットのタンバル・ヴィルヌーブ {#timbales-d-amourettes-villeneuve}

温製オードブル参照。

<!--
\index{amourettes|)}
\index{あむれつと|)}
-->

</div><!--endRecette-->


### セルヴェル

<!--
\index{cervelle|(}
\index{せるうえる@セルヴェル|(}
\index{のう@脳|see {セルヴェル}}
-->


<div class="recette">

#### セルヴェル・ブールノワール {#cervelle-au-beurre-noir}

\index{cervelle@cervelle!beurre noire@--- au beurre noir}
\index{beurre noir@beurre noirl!cervelle@Cervelle au ---}
\index{せるうえる@セルヴェル!ふるのわる@---・ブールノワール}
\index{ふるのわる@ブールノワール!せるうえる@セルヴェル・---}

<div class="frsubenv">Cervelle au beurre noir</div>

セルヴェルを[エスカロップ\*](#escalope-gls)に切る。丸皿にターバン状に
盛り付ける。その中心にアムレットの[トロンソン\*](#troncon-gls)を盛り込
む。

塩と胡椒で調味する。提供直前に、150gのブール・ノワールに、25gのパセリ
の葉を一気に加え、セルヴェルとアムレット全体を覆うようにかける。

完全に熱したフライパンでヴィネガーを瞬時に煮立たせ素早くひと垂らしして仕上げる。

\atoaki{}

#### セルヴェル・ブールノワゼット {#cervelle-au-beurre-noisette}

\index{cervelle@cervelle!beurre noisette@--- au beurre noisette}
\index{beurre noisette@beurre noisette!cervelle@Cervelle au ---}
\index{せるうえる@セルヴェル!ふるのわせつと@---・ブールノワゼット}
\index{ふるのわせつと@ブールノワゼット!せるうえる@セルヴェル・---}

<div class="frsubenv">Cervelle au beurre noisette</div>

セルヴェルを[エスカロップ\*](#escalope-gls)に切る。アムレットは[トロン
ソン\*](#troncon-gls)とともに上記のレシピとおなじように盛り付ける。

味付けをし、150gのブール・ノワゼットを全体を覆うようにかける。仕上げに
パセリアッシェとレモン汁をひと垂らしセルヴェルの上にかける。


\atoaki{}

#### セルヴェル・ブルゴーニュ風  {#cervelle-a-la-bourguignonne}

\index{cervelle@cervelle!bourguignonne@--- à la bourguignonne}
\index{bourguignon@bourguignon(ne)!cervelle@Cervelle à la ---ne}
\index{せるうえる@セルヴェル!ふるこにゆふう@---・ブルゴーニュ風}
\index{ふるこにゆふう@ブルゴーニュ風!せるうえる@セルヴェル・---}

<div class="frsubenv">Cervelle à la Bourguignonne</div>

セルヴェルを[エスカロップ\*](#escalope-gls)に切る。そこに、マッシュルームと小玉葱のグラッセをガルニチュールとして加える。
ブルギニヨンと呼ばれる赤ワインソースをかぶるくらいまでかける。７、８分弱火で加熱する。

深皿に盛り付ける。澄ましバターで、揚げ焼きにした小さなハート型のクルトンで、周囲を囲む。

\atoaki{}

#### セルヴェルのコキーユ[^coquille]・グラタン仕立て  {#coquilles-de-cervelle-au-gratin}

\index{cervelle@cervelle!coquille gratin@Coquille de --- au gratin}
\index{coquille@coquille!cervelle gratin@--- de Cervelle au gratin}
\index{gratin@gratin!coquille coquille@Coquille de Cervelle au ----}
\index{せるうえる@セルヴェル!こきゆはりふう@---のコキーユ・パリ風}
\index{はりふう@パリ風!せるうえるこきゆ@セルヴェルのコキーユ・---}
\index{こきゆ@コキーユ!せるうえるはりふう@セルヴェルの---・パリ風}
<div class="frsubenv">Coquille de Cervelle au Gratin</div>

コキーユの縁にジャガイモ・デュシェスを絞る。殻の底にソース・デュクセル少量を塗る。
ポシェしたセルヴェルの[エスカロップ\*](#escalope-gls)と、大きなマッシュルームの薄切りを火入れして盛り込む。
小さなマッシュルームを中央に置き、やや固めに仕上げたソース・デュクセルをかぶせかける。
パン粉をまぶす。溶かしバターをアロゼする。強火のオーブンで焼き色をつける。

オーブンからコキーユを出したら、パセリのアッシェをふりかけ、何滴かのレモン汁をかける。

[^coquille]: 貝殻、ここではホタテの貝殻のこと

\atoaki{}


#### セルヴェルのコキーユ・パリ風  {#coquilles-de-cervelle-a-la-parisienne}

\index{cervelle@cervelle!coquille parisienne@Coquille de --- à la Parisienne}
\index{coquille@coquille!cervelle parisienne@--- de Cervelle à la Parisienne}
\index{parisien@parisien(ne)!coquille cervelle@Coquille de Cervelle à la ----ne}
\index{せるうえる@セルヴェル!こきゆはりふう@---のコキーユ・パリ風}
\index{はりふう@パリ風!せるうえるこきゆ@セルヴェルのコキーユ・---}
\index{こきゆ@コキーユ!せるうえるはりふう@セルヴェルの---・パリ風}


<div class="frsubenv">Coquilles de Cervelle à la Parisienne</div>

コキーユを上記のように縁取りする。溶いた卵黄を塗り、オーブンで色付ける。

[ソース・アルモンド](#sauce-allemande)少々を殻に塗る。セルヴェルを薄くエスカロップにし、盛り込む。
マッシュルームとトリュフの薄切り何枚かを冠状に並べる。中央に小さなマッシュルームを置く。
ソース・アルモンドにバターを加え、覆いかける。焼き色をつける。

 \atoaki{}

#### セルヴェルのクロメスキとクロケット  {#crosmesquis-et-croquettes-de-cervelle}

<div class="frsubenv">Cromesquis et Croquettes de Cervelle</div>

これらの作り方についてはアムレットの項で示したように作る。

\atoaki{}

#### セルヴェルのフリトー  {#fritot-de-cervelle}

<div class="frsubenv">Fritot de Cervelle</div>

セルヴェルをエスカロップに切る。マリネする。アムレットのフリトーで示したように作業する。。

\atoaki{}

#### セルヴェルのイタリア風 {#cervelle-a-l-italienne}

<div class="frsubenv">Cervelle à l'Italienne</div>

生のセルヴェルを大きめの薄いエスカロップにする。塩コショウする。小麦粉の中で巻く。バターと植物油半々でソテーする。

温かい皿に冠状に並べる。中央に[ソース・イタリエンヌ](#sauce-italienne)を流す。

\atoaki{}




#### セルヴェルのマリナード[^10] {#marinade-de-cervelle}

\index{cervelle@cervelle!marinade@Marinade de ---}
\index{marinade@marinade!cervelle@--- de Cervelle}
\index{せるうえる@セルヴェル!まりなと@---のマリナード}
\index{まりなと@マリナード!せるうえる@セルヴェルの---}

<div class="frsubenv">Marinade de Cervelle (1-4)</div>

セルヴェルをエスカロップにし、フリトーのようにマリネする。粉をまぶし、卵を塗り、パン粉の衣を着せて[^8]、提供する直前に揚げる。
ナフキンの上に盛り、揚げたパセリを添える。

[ソーストマト](#sauce-tomate)を別添で供する。

[^8]: 原文 paner à l'anglaise （パネアロングレーズ）イギリス式パン粉衣とも呼ばれる。

[^10]: すでに[マリナードとソミュール](#marinade-et-saumure)の訳注で見た
    ように、この料理はとても古くから存在している。

\atoaki{}

#### セルヴェルのマトロット {#matelote-de-cervelle}

\index{cervelle@cervelle!matelote@Matelote de ---}
\index{matelote@matelote!cervelle@--- de Cervelle}
\index{せるうえる@セルヴェル!まとろつと@---のマトロット}
\index{まとろつと@マトロット!せるうえる@セルヴェルの---}

<div class="frsubenv">Matelote de Cervelle (1-4)</div>

セルヴェルをしっかり血抜きし、前もってしっかり香りを移した[赤ワインのクールブイヨン](#court-bouillon-c)で火を入れる。
そしてエスカロップにし、小さなマッシュルームと小玉葱のグラセを加える。
クールブイヨンを濾し、煮詰め、[ブールマニエ](#beurre-manie)でとろみを付ける。深皿に盛る。バターで揚げた小さなハート型のクルトンを添える。


###### 【原注】{#nota-matelote-de-cervelle}

このマトロットは、火入れの方法以外は、上記のセルヴェル・ブルゴーニュ風とまったく同一の料理だ。

\atoaki{}


#### セルヴェルのマザグラン[^11] {#mazagran-de-cervelle}

\index{cervelle@cervelle!mazagran@Mazagran de ---}
\index{mazagran@mazagran!cervelle@--- de Cervelle}
\index{せるうえる@セルヴェル!まざぐらん@---のマザグラン}
\index{まとろつと@マトロット!せるうえる@セルヴェルの---}

<div class="frsubenv">Mazagran de Cervelle (1-4)</div>

楕円の平皿、または丸皿にたっぷりとバターを塗り、[チュルボのクリームグ
ラタン](#turbot-creme-gratin)のように、縁にじゃがいも・デュシェスを絞
る。ただし、厚みはより薄く、そして少し高くすること。

中央にポシェしたセルヴェルのエスカロップとマッシュルームとトリュフのス
ライス、マッシュルームの茹で汁を煮詰めて加えたソース・アルマンドでリエ
したラグーを盛り込む。

薄く伸ばしたじゃがいも・デュシェスを被せる。手早く縁をしっかりと貼りつ
ける。溶き卵を塗り、前もって準備しておいた、細かいじゃがいも・デュシェ
スの飾りを下方に配する。たんにフォークで筋をつけてもいい。強火のオーブ
ンで色付ける。

マザグランをオーブンから出したら、周囲を取り囲むようにトマトソースを帯
状に流す。前もってグリル焼きしておいた小さなシポラタソーセージを冠状に
配置する。

[^11]: 基本的には、じゃがいも・デュシェスで作った入れ物に、小っさくさ
いの目に刻んだ野菜を詰めたタルトレットのようなものか、バターとじゃがい
も・デュシェスでつくったタルトレットにさいの目に刻んだ材料を詰めてオー
ブンで焼いたもの、を指す。ここでは後者の大きな仕立てを呼ぶべきところだ
ろう。

\atoaki{}

#### セルヴェルのムスリーヌ {#mousseline-de-cervelle}

\index{cervelle@cervelle!mousseline@Mousseline de ---}
\index{mousseline@mousseline!cervelle@--- de Cervelle}
\index{せるうえる@セルヴェル!むすりぬ@---のムースリーヌ}
\index{むすりぬ@ムスリーヌ!せるうえる@セルヴェルの---}

<div class="frsubenv">Mousseline de Cervelle</div>

ごく白いセルヴェルをバターで[エチュヴェ\*](#etuver-gls)する。ジャガイモのピュレと一緒に
細かな網で裏\ruby{漉}{ご}し、ポタージュ用のロワイヤルの割合で[アパレイ
ユ\*](#appareil-gls)を準備する。

バターをたっぷり塗った型に流し入れ、湯煎で[ポシェ\*](#pocher-gls)する。
７、8分休ませてから、型から抜いて盛り付ける。

別添で、薄くスライスしたマッシュルームに火入れして加えた、白い[ソース・
クレーム](#sauce-creme)を供する。

\atoaki{}

#### セルヴェルのパン[^12]・ヴィラジョワーズ[^13] {#pain-de-cervelle-a-la-villageoise}

\index{cervelle@cervelle!pain@pain de --- à la villageoise}
\index{villageois@villageois(e)!pain cervelle@pain de cervelle à la ---e}
\index{pain@pain!cervelle villageoise@Pain de Cervelle à la Villageoise}
\index{せるうえる@セルヴェル!はん ういらしおわす@---のパン・ヴィラジョワーズ}
\index{はん@パン!せるうえる ういらしおわす@セルヴェルの---・ヴィラジョワーズ}
\index{ういらしよわす@ヴィラジョワーズ!せるうえる はん@---のパン・ヴィラジョワーズ}


<div class="frsubenv">Pain de Cervelle à la Villageoise</div>

セルヴェル 400 g をバターで[エチュヴェ\*](#etuver-gls)する。鉢ですりつ
ぶし、100gのバター、200gの[パナード・フランジパー
ヌ](#panade-c)、塩、こしょう、ナツメグ、を同じ割合で混ぜたも
のを合わせて混ぜ込み、卵 3個を 1 つずつ加えてゆく。細かな網で裏
\ruby{漉}{ご}し、ヘラで滑らかに練る。バターを塗った型にプレーンな丸型
の口金を使い流し込む。湯\ruby{煎}{せん}にかけ30分から35分間[ポシェ
\*](#pocher-gls)する。

型から外す前に、中身が軽く締まるよう７分から8分間パンを休ませる。
火入れたマッシュルームのスライスが入ったヴルーテを別添。

###### 【原注】 {#nota-pain-de-cervelle-a-la-villageoise}

<!--ノート（すいません。訳せません。）
ポシェする時間を示さないのは常に１Lの容量の型を使った場合。提供するための詳細ーアパレイユは、おそらくダリオール型、または、それと同じ容量のもの
を使用しポシェする。その型だと12から14分のポシェとなる。-->

ポシェの時間は特に指定がない場合、内容量 1 L の型を使う前提。小分けにして
供する場合には、ダリオル型に詰めてポシェするが、別の型でもいい。その場
合、ポシェの時間は 12 〜 14 分となる。

[^12]: いわゆる小麦粉でつくるパンのことではなく、\kenten{パンのような
    塊（かたまり）}という意味での料理名。
    
    
[^13]: 村人風、村の、の意味だが、かならずしも素朴な料理に冠されないこ
    ともあるので留意しておきたい。この料理もレシピそのものは素朴だが、
    19世紀的な装飾をすればかなり豪華な仕立てとなるだろう。

\atoaki{}





#### セルヴェル・プレット {#cervelle-a-la-poulette}

\index{cervelle@cervelle!poulette@--- à la Poulette} <!--索引のマークアップはしないで結構です-->
<!--\index{} 僕が索引をつくるときは前のレシピで近い形態のものをコピペしますので不要です。というかデバッグ対象として非常に厄介な存在になりますので、書かないでください-->
\index{poulette@poulette!cervelle@cervelle à la ---}
\index{せるうえる@セルヴェル!ふれつと@---・プレット}
\index{ふれつと@プレット!せるうえる@セルヴェル・---}


<div class="frsubenv">Cervelle à la Poulette</div>

<!--セルヴェルを半生にポシェし、エスカロップにする。ソースアラプーレッ
トの中でさっとソテーし、タンバルの中に盛り付ける。-->ポシェしたてのセ
ルヴェルを[エスカロップ\*](#escalope-gls)に切る。[ソース・プレッ
ト](#sauce-poulette)の中で軽くソテーし[^14]、深皿に盛り付ける。

[^14]: 直訳。意味としては、さっとソテーしてソース・プレットを注いでな
    じませる、と理解したいところ。
    
\atoaki{}

#### セルヴェル・ラヴィゴット {#cervelle-a-la-ravigote}

\index{cervelle@cervelle!ravigote@--- à la ravigote}
\index{ravigote@ravigote!cervelle@Cervelle à la ---}
\index{せるうえる@セルヴェル!らういこつと@---・ラヴィゴット}
\index{らういこつと@ラヴィゴット!せるうえる@セルヴェル・---}


<div class="frsubenv">Cervelle à la Ravigote</div>

提供する皿の縁にじゃがいも・デュシェスを絞り、オーブンで軽く色付けておく。<!--段落変えはカラ行をひとつ入れてください-->

セルヴェルを[ポシェ\*](#pocher-gls)してすぐに[エスカロップ
\*](#escalope-gls)にし、皿の中央に盛り込む。[ソース・ラヴィゴッ
ト](#sauce-ravigote)を\ruby{覆}{おお}いかける。

\atoaki{}

#### セルヴェルのスフレ・ケース仕立て[^15]　 {#souffles-de-cervelle-en-casses}

\index{cervelle@cervelle!souffle@Soufflés de --- en Caisse}
\index{souffle@soufflé!cervelle@--- de Cervelle en Caisses}
\index{せるうえる@セルヴェル!するれけす@---のスフレ・ケース仕立て}
\index{すふれ@スフレ!せるうえるけす@セルヴェルの---・ケース仕立て}

<div class="frsubenv">Sufflés de Cervelle en Caisses</div>

ケース20個あたり、以下の[アパレイユ\*](#appareil-gls)を用意する。セルヴェル500gを生のまま
[エスカロップ\*](#escalope-gls)にする。バターで[ポシェ\*](#pocher-gls)し、細かな網で裏ごしする。よく煮詰め
た[ベシャメルソース](#sauce-bechamel) 2 $\frac{1}{2}$ dL、塩こしょう、卵黄 5 個を混ぜ込む。卵白 5
個分は固く泡立てて混ぜる。

絞り袋を使ってケースに絞り込み、弱火のオーブンで12分間火入れる。
オーブンからスフレを出したらすぐに折りたたんだナフキンに盛り付け、供する。

[^15]: 空焼きした[タルトレット](#tartelettes)や[ブシェ](#bouchees)にア
    パレイユを詰めて焼き、スフレにする。


\atoaki{}

#### セルヴェルのスュブリック {#subrics-de-cervelle}

\index{cervelle@cervelle!subric@Subrics de ---}
\index{subric@subric!cervelle@---s de Cervelle}
\index{せるうえる@セルヴェル!すゆふりつく@---のスュブリック}
\index{すゆふりつく@スュブリック!せるうえる@セルヴェルの---}
\index{しゆふりつく@シュブリック|see {スュブリック}}

<div class="frsubenv">Subrics de Cervelle </div>

[ポシェ\*](#pocher-gls)したセルヴェル 500 g を大きめのさいの目に切る。この[サルピコン
\*](#salpicon-gls)を煮詰めた[ソース・アルマンド](#sauce-allemande) 1
dL に全卵3個を加え、[リエ\*](#lier-gls)する。そのまま冷ます。

フライパンで澄ましバターをよく熱する。先程の[アパレイユ
\*](#appareil-gls)を大さじですくい、大きめのマカロンのような形になるよ
うにして、その中へ落とす。これがスュブリックとなる。下の面が色づいたら
パレットナイフを使い慎重に裏返えす。その面も色付け、熱した丸皿に冠状に
盛り付ける。

別添でソース・トマトを供する。


\atoaki{}

#### セルヴェル・ヴィルロワ {#cervelle-villeroy}

\index{cervelle@cervelle!villeroy@--- Villeroy}
\index{villeroy@villeroy!cervelle@Cervelle ---}
\index{せるうえる@セルヴェル!ういるろわ@---・ヴィルロワ}
\index{ういるろわ@ヴィルロワ!せるうえる@セルヴェル・---}

<div class="frsubenv"> Cervelle  villeroy</div>

生のセルヴェルを[エスカロップ\*](#escalope-gls)にする。塩こしょうし、
バターでポシェする。すぐに[ソース・ヴィルロワ](#sauce-villeroy)に浸し
て冷ましておく。

イギリス式にパン粉衣を付け[^16]、提供直前に揚げる。ナフキンの上に盛り、揚げたパセリのブーケを添える。

軽く仕上げた[ソース・ペリグー](#sauce-perigueux)を別添で供する。

[^16]: 小麦粉、溶き卵、パン粉の順で衣を付けるが、パン粉は細かいものを使う。

<!--
\index{cervelle|)}
\index{せるうえる@セルヴェル|)}
-->


</div><!--endRecette--> <!--この行がかならず二段組の最後に来るようにしてください-->

<div class="main"> <!--ここから1段組-->

### コントルフィレ[^17] {#contrefilet}

\index{contrefilet}
\index{こんとるふぃれ@コントルフィレ}
\index{さろいん@サーロイン|see {コントルフィレ}}
\index{ふおふいれ@フォーフィレ|see {コントルフィレ}}


コントルフィレはオンプ[^18]の先から最初の肋骨までの背骨に沿った部位の
こと。調理法はフィレとおなじで、フィレのレシピはどれも当てはまる。

[ブレゼ](#les-braises)する場合は[デゾセ\*](#desosser-gls)するが、ロー
ストの場合はイギリス式に骨を残しておいたほうがいい。

その場合、いくつかの部分に分けている太い筋は取り除いて、変形を防ぐこと。
背骨は根本近くで割り、切り分けやすいようにしてやること。

コントルフィレは、とりわけ上質のものの場合、ローストにするのがいい。



[^17]: contrefilet (コントゥルフィレ)、 faux filet （フォフィレ）とも。いわゆるサーロインに相当する部位。

[^18]: いわゆるハラミ（サガリ）。

### \hspace*{-1zw}コントルフィレの冷製 {#contrefilet-froid}

\index{contrefilet@contrefilet!froid@--- froid}
\index{こんとるふぃれ@コントルフィレ!れいせい@---の冷製}

コントルフィレを冷製で供さねばならない場合、ますは注意深く形状を整えて、
ジュレを塗り、皿の周囲にジュレのクルトン[^20]を配し、盛り付けたら周囲に[ア
シェ\*](#hacher-gls)した[ジュレ](#gelee-ordinaire)を飾る[^19]。

[^19]: 現代において主流となっているような柔らかいジュレの場合は波模様
    の口金を付けた絞り袋を用いてもいい。
    
[^20]: 角切りにしたジュレのこともクルトンと呼ぶ。

### \hspace*{-1zw}オントルコット[^21] {#entrecote}

<div class="center">（約 10 人分……1 塊 400 g のオントルコット 3 つ）</div>

その名称のとおり、オントルコットは 2 本の肋骨の間にある肉のことを意味
するが、一般に小分けして供する場合はコントルフィレの一部と見なされる。
だから、本書ではコントルフィレの派生型として位置付けることとした。

[^21]: entrecôte （オントゥルコット）。いわゆるリブロースのこと。
 
 
</div><!--endMain--> <!--ここが1段組み終了の印です-->
 
<div class="recette">

#### オントルコット・ベアルネーズ {#entrecote-a-la-bearnaise}

\index{entrecote@entrecôte!bearnaise@--- à la Béarnaise}
\index{bearnais@béarnais(e)!eontrecote@Entrecôte à la ---}
\index{おんとるこつと@オントルコット!へあるねーすお@---・ベアルネーズ}
\index{へあるね@ベアルネ / ベアルネーズ!おんとるこつと@オントルコット・ベアルネーズ}

 
<div class="frsubenv">Entrecôte à la béarnaise</div>
 
オントルコットをグリルで焼き、温めた長皿に盛り付ける。それぞれの端にジャ
ガイモのシャトーをまとめて配する。オントルコットにブロンド色の[グラス
ドビヤンド](#glace-de-viande)を軽く塗る。周囲に[ソース・ベアルネー
ズ](#sauce-bearnaise)を帯状にたっぷり流す。
 
###### 【原注】 {#nota-entrecote-a-la-bearnaise}
 
オントルコットの皿にソースをかけなくてもいいが、その場合はソース・ベア
ルネーズをソース入れで別添にする。
 
\atoaki{}

#### オントルコット・ベルシー {#entrecote-a-la-bercy}

\index{entrecote@entrecôte!bercy@--- à la Bercy}
\index{bercy@Bercy!entrecote@Entrecôte à la ---}
\index{おんとるこつと@オントルコット!へるし@---・ベルシー}
\index{へるし@ベルシー!おんとるこつと@オントルコット・---}

<div class="frsubenv">Entrecôte à la bercy</div>

オントルコットをグリル焼きする。ブロンド色のグラスドビヤンドを軽く塗り、
[ブール・ベルシー](#beurre-bercy)150 g をのせたサービス用の皿に盛り付
ける。


###### 【原注】 {#nota-entrecote-a-la-bercy}

ブール・ベルシを柔らかくしてからオントルコットの上にのせる店もある。ま
た、半ば溶けた状態をソース入れに別添で供する店もある。これといって特に
決まりはない。

\atoaki{}

#### オントルコット・ボルドー風 {#entrecote-a-la-bordlaise}

\index{entrecote@entrecôte!bordelaise@--- à la bordelaise}
\index{bordelais@bordelais(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!ほるとふう@---ボルドー風}
\index{ほるとふう@ボルドー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte à la Bordelaise</div>

オントルコットをバターでソテーし、温めた長皿に盛り付ける。それぞれのア
ントルコットの中心に、大きめのモワルの薄切りを[ポシェ\*](#pocher-gls)
して一列に配する。[ソース・ボルドー風](#sauce-bordelaise)を別添で供す
る。

###### 【原注】 {#nota-entrecote-à-la-bordlaise}

赤ワインのソース・ボルドー風を使うやり方はいい。だがその間違いをもここ
でもう一度指摘しておきたい。本物のボルドー風は、白ワインを使う。それは
かつて、ソース・ボヌフォワ・ボルドー風と呼ばれていたものだ[^23]。

[^23]: [ボルドー風ソース](#sauce-bordelaise)参照。
\atoaki{}

#### オントルコット・マッシュルーム添え {#entrecote-aux-champignons}

\index{entrecote@entrecôte!champignons@--- aux champignons}
\index{champignon@champignon!entrecote@Entrecôte aux ---s}
\index{おんとるこつと@オントルコット!まつしゆるむそえ@---マッシュルーム添え}
\index{まつしゆるむそえ@マッシュルーム添え!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte aux champignons</div>

オントルコットをバターでソテーし盛り付ける。マッシュルームの茹で汁 1
dLでソテー鍋を[デグラセ](#deglacer-gls)し、その中に、茹でた小さなマッ
シュルームの頭20個を加え、オントルコットの上に並べる。

デグラセした汁の中に[ソース・デミグラス](#sauce-demi-glace) 1
$\frac{1}{2}$ dL を加え、半分に煮詰め、シノワで濾す。

そのソースを 50 〜 60 g のバターでモンテし、オントルコットに軽く塗り、
残りのソースは別添で供する。

\atoaki{}

#### オントルコット・フォレスティエール[^22] {#entrecote-a-la-forestiere}

\index{entrecote@entrecôte!forestiere@--- à la forestière}
\index{forestiere@forestier / forestière!entrecote@Entrecôte à la Forestière}
\index{おんとるこつと@オントルコット!ふおれすていえる@---・フォレスティエール}
\index{ふおれすていえる@フォレスティエール!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte à la Forestière</div>

オントルコットをバターでソテーし、盛り付ける。モリーユ茸 300 g と、大きめの
さいの目切りにしたじゃがいも 300 g をバターでソテーする。[ブロンシー
ル](#blanchir-gls)し、[リソレ](#rissoler-gls)した塩漬け豚ばら肉を三角
形に盛り、それと交互になるように上記にブーケのように並べる。モリーユとジャ
ガイモのブーケの上に少量のパセリの[アシェ\*](#hacher-gls)を散らす。

ソテー鍋をスプーン何杯かの白ワインでデグラセする。美味しい[仔牛の
ジュ](#jus-de-veau-lie)を 1,5 $\frac{1}{2}$ dL 加え、漉してから別添で供
する。
はん
[^22]: forestier （フォレスティエ）、forestière （フォレスティエール）。
森番、森林管理人、木こり、の意。


\atoaki{}

#### オントルコット・ハンガリー風 {#entrecote-a-la-hongroise}

\index{entrecote@entrecôte!hongroise@--- à la Hongroise}
\index{hongrois@hongrois(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte à la Hongroise</div>

オントルコットをバターでソテーしながら、バターをパッセする。125gの豚バラ肉を賽の目に切り、[ブロンシー
ル](#blanchir-gls)する。中くらいの玉ねぎ2個をアッシェし、よく色付ける。一つまみのパプリカと１dlの白ワインを加え、2/3まで煮詰め、
そ2,5dlのヴルーテを加え、7~8分間火を入れる。
提供直前にオントルコットにそのソースを覆いかけ、その周りに球形のジャガイモを水で火を入れて並べる。


\atoaki{}

#### オントルコット・オトゥリエール　 {#entrecote-a-l-hoteliere}

\index{entrecote@entrecôte!hoteliere@--- à l'hôtelière}
\index{hotelier@hôtelier / hôtelière!entrecote@Entrecôte à l'hôtelière}
\index{おんとるこつと@オントルコット!おとうりえる@---・オトゥリエール}
\index{おとうりえる@オトゥリエール!おんとるこつと@オントルコット・---}

<div class="frsubenv">Entrecôte à l'Hôtelière</div>

オントルコットをバターでソテーし、玉ねぎとエシャロットで調味し強めに乾燥させたデュクセルをスープスプーン1杯加えた150gのメートルドテルバターで覆う。

ソトワールを1,5dlの白ワインでデグラセし、2/3まで煮詰め、溶けたグラスドヴィアンドをスプーン１杯加えて、オントルコットに掛ける。

\atoaki{}

####　オントルコット・リヨン風{#entrecote-a-la-}

\index{entrecote@entrecôte!hongroise@--- à la Hongroise}
\index{hongrois@hongrois(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte à la Lyonnaise</div>
オントルコットをバターでソテーし、盛り皿に盛り付ける。
300gの玉葱を薄くスライスし、バターでソテーし火が入ったら、スプーン一杯の溶けたグラスドヴィアンドでリエ、軽くブーレし、脇に付け合わせる。
軽くパセリアッシェを振りかける。

1.5dlの白ワインと一垂らしのビネガーでソトワールをデグラセし、2/3まで煮詰める。ソース・デミグラスを1dl加え、シノワで漉す。
50gのバターでそのソースをモンテし、オントルコットに流し掛ける。

\atoaki{}

####　オントルコット・マルシャンドヴァン{#entrecote-}

\index{entrecote@entrecôte!hongroise@--- à la Hongroise}
\index{hongrois@hongrois(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte Marchand de vins.</div>
オントルコットをグリエし、柔らかくしたブール・マルシャンドヴァンで覆う。または提供時に別添。
（ブール・コンポゼのソースの項を参照すること）

\atoaki{}

####　オントルコット・マルセイユ風{#entrecote-a-la-}

\index{entrecote@entrecôte!hongroise@--- à la Hongroise}
\index{hongrois@hongrois(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte a la Marseillaise.</div>

オントルコットをグリエし、メートルドテルバターにその1/4のブールドトマトと大蒜の先端を砕いて足したものを覆い掛ける。

ジャガイモのコポーと小さな半分のトマトをガルニチュール・マルセイエーズのように準備し、取り囲む。

\atoaki{}

####　オントルコット・メキシコ風{#entrecote-a-la-}

\index{entrecote@entrecôte!hongroise@--- à la Hongroise}
\index{hongrois@hongrois(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte a la Mexicaine.</div>

オントルコットをグリエし、盛り付ける。
ガルニチュール・メキシケーヌのように準備したきのことピーマンのグリエで囲む。
十分に味を際立たせたトマトのジュを別添。

\atoaki{}

####　オントルコット・ミラボー風{#entrecote-a-la-}

\index{entrecote@entrecôte!hongroise@--- à la Hongroise}
\index{hongrois@hongrois(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte a la Mirabeau.</div>

オントルコットをグリエし、種を抜いたオリーブを加えたアンチョビフィレの千切りのグリエを敷いた上に盛り付ける。
ブランシールしたエストラゴンの葉をオントルコットの縁に飾る。
ブール・ダンショワを別添。

\atoaki{}

####　オントルコット・チロル風{#entrecote-a-la-}

\index{entrecote@entrecôte!hongroise@--- à la Hongroise}
\index{hongrois@hongrois(e)!entrecote@Entrecôte à la ---e}
\index{おんとるこつと@オントルコット!はんかりふう@---・ハンガリー風}
\index{はんかりふう@ハンガリー風!おんとるこつと@オントルコット---}

<div class="frsubenv">Entrecôte a la tyrolienne.</div>

オントルコットをグリエする。
玉葱250gを薄くエマンセし、バターで揚げ、少量のソースポワヴラードでリエし、その上にオントルコットを盛り付ける。
その周囲にフォンデュドトマトを紐のように少量垂らす。





</div><!--endRecette--> <!--この行がかならず二段組の最後に来るようにしてください-->










