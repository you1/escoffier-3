---
author: '五 島　学　訳・注'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
  pararecette:
  - pararecette
title: 'エスコフィエ『フランス料理の手引き』'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->


<div class="main">

\thispagestyle{empty}


## 冷たいフルーツのオントルメ

### \hspace*{-1zw}アプリコット {#abricots-entremets-froids}


<div class="recette">

#### アプリコット・ミレイユ {#abricots-mireille}


<div class="frsubenv">Abricots Mireille</div>


よく熟れた大きめのアプリコットの皮を剥き、2 つに切る。深さのある型に薄皮を\ruby{剥}{む}いたアーモンドと一緒に詰めて、
砂糖をふって前もって氷の上に 1 時間置いておく。
提供直前に、キルシュで[アロゼ\*](#arroser-gls)し、砂糖を加えたバニラ風味の[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)で
\ruby{覆}{おお}って、表面にジャスミンの花とプロヴァンスのスミレの花入りの飴を散りばめる。

\atoaki{}

#### アプリコット・パリ風 {#abricots-a-la-parisienne}


<div class="frsubenv">Abricots à la Parisienne</div>

2 つに切ったアプリコットをバニラ風味のシロップで[ポシェ\*](#pocher-gls)する。冷まして水気を切り、
くるみ位の大きさの[バニラアイス](#glace-creme-a-la-vanille)を中に詰めて、半割りのアプリコットを元の形に戻す。

大きめのマカロンを裏返してアプリコットを載せ、バニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)で
アイスを\ruby{覆}{おお}うように円錐型に盛って、細かいヘーゼルナッツのプラランをまぶす。

\atoaki{}

#### アプリコット・ロワイヤル {#abricots-a-la-royale}


<div class="frsubenv">Abricots à la Royale</div>

半割りにした大きめのアプリコットをバニラ風味のシロップで[ポシェ\*](#pocher-gls)して冷ましておき、
少し深さのあるタルトレット型に置く。
とても澄んだキルシュ風味のジュレを型一杯に入れて仕上げる。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}パイナップル {#ananas-entremets-froids}

<div class="recette">

#### パイナップル・ジョルジェット {#ananas-georgette}


<div class="frsubenv">Ananas Georgette</div>

丸ごとの大きめのパイナップルを、底面と側面の果肉を厚さ 1 cm 残して取る。
葉を付けたまま、上の部分を輪切りにして取っておく。
パイナップルのピュレの[ムース・グラッセのアパレイユ](#composition-de-mousse-glaces-aux-fruits)に、
取り出したパイナップルを薄切りにして加えたものを、中に一杯に詰める。

ナフキンの上に置いて、取っておいた上の部分を載せ、元のパイナップルの形に戻す。

\atoaki{}

#### パイナップル・ニニョン {#ananas-ninon}


<div class="frsubenv">Ananas Ninon</div>

スフレ用の鍋の側面に[バニラアイス](#glace-creme-a-la-vanille)を斜めに置く、
つまり、鍋の中央の空洞が逆円錐型になるように、鍋の縁から底の中央に下りるような形にアイスを置く。

このアイスの層の上に、新鮮なパイナップルの薄切りを 2、3 枚置き、最後に置くパイナップルが鍋の縁からはみ出るようにする。

型の中央に、フレーズ・デ・ボワをピラミッド状に並べて置き、フランボワーズのピュレで\ruby{覆}{おお}って
ピスタチオの[アシェ\*](#hacher-gls)を散らす。


\atoaki{}

#### パイナップル・ロワイヤル  {#ananas-a-la-royale}


<div class="frsubenv">Ananas à la Royale</div>

新鮮なパイナップルを掃除して、上の葉の束はそのまま取っておく。

底面と側面に厚さ約 1 cm の果肉を残すように、中身を取り除く。

キルシュ風味の新鮮な[フルーツのマセドワーヌ](#macedoine-de-fruits-rafraichis)を詰めて、
クリスタル製の容器の中央にパイナップルを置き、バニラ風味のシロップで[ポシェ\*](#pocher-gls)した
モントルイユ産の大きめのペッシュと、キルシュで[マセレ\*](#macerer-gls)しておいた大きめのいちごを、
交互にパイナップルの周りに 1 周並べる。

葉の部分をパイナップルの元の場所に戻す。 


\atoaki{}


#### パイナップル・バルジニ  {#ananas-virginie}


<div class="frsubenv">Ananas Virginie</div>

[パイナップル・ジョルジェット](#ananas-georgette)のパイナップルのムースをいちごのムースに代えて、
同じようにくり抜いたパイナップルの果肉の角切りを加え、あとは同じように作る。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}バナナ {#bananes-entremets-froids}

<div class="recette">

#### バナナのサラダ  {#bananes-en-salade}


<div class="frsubenv">Bananes en Salade</div>

適度にやわらかく少し引き締まったバナナを使う。皮を剥き果肉を輪切りにする。
皿に並べ、オレンジ風味の砂糖少々を混ぜた砂糖をふって、そのまま $\frac{1}{4}$ 時間[マセレ\*](#macerer-gls)しておく。

提供直前に、サラダボウルに入れ、キルシュをふりかけて輪切りのバナナの形が崩れないように丁寧に混ぜる。

###### 【原注】 {#nota-bananes-en-salade}

オレンジ風味の砂糖を、非常に細く切り[ブロンシール\*](#blanchir-gls)してシロップで数分間茹でた
[ゼスト\*](#zeste-gls)に代えても良い。

\atoaki{}


#### バナナ・トレデルヌ  {#bananes-tredern}


<div class="frsubenv">Bananes Traders</div>

やわらか過ぎないバナナを中心に沿って割って、果肉を取り、皮を取っておく。
バナナの半分をバニラ風味のシロップで[ポシェ\*](#pocher-gls)し、水気をきって冷まして
[フィレの状態に加熱した](#cuisson-du-sucre)アプリコットを表面に塗る。
フルーツの砂糖漬けで飾る。

残りのバナナを網などで裏ごし、そのピュレ $\frac{1}{3}$ 量あたりキルシュ風味の
[モスコヴィット](#moscovite-aux-fruits)の[アパレイユ\*](#appareil-gls)を $\frac{2}{3}$ 量の割合で加える。
この[アパレイユ\*](#appareil-gls)をバナナの皮に詰め、容器に入れた氷[^104]に置く。
ナフキンに盛って、詰め物をした皮それぞれに、飾り付けた片方のバナナをのせる。


[^104]: 原文rafraîchissoir（ラフレシソワール）＝ rafraîchisseur（ラフレシスール）、
ワインやキャビアなどのクーラーのように、氷やクラッシュアイスなどを入れて、冷やすための容器。


\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}さくらんぼ

<div class="recette">

#### さくらんぼ・デュバリー  {#cerises-dubarry}


<div class="frsubenv">Cerises Dubarry</div>

フラン用のセルクルに[軽いフォンセ生地](#pate-a-foncer-fine)を貼り、タルト型に載せて、
焼いている最中に膨らまないように、生地の底に小さな穴を沢山あけ、粉糖をふり、
種を取った大きめのさくらんぼを\ruby{隙間}{すきま}なく敷き詰める。

標準的な方法でフランを焼いて、そのまま冷ます。

しっかりと冷えたら、標準的なプラランかまたは砕いたマカロン[^105]を加えた[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)
でさくらんぼ全体を\ruby{覆}{おお}う。

クリームの表面をフランの縁に沿ってならし、粉状のマカロンを全体にまぶしたら、白色とピンク色にした
[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)をコルネで絞って飾る。


[^105]: 現代日本で一般的なふんわりと柔らかいマカロンではなく、マカロン・クラクレに代表される
堅いタイプのマカロン、すなわちアーモンド粉末を ベースとした歯ごたえのある焼き菓子を想定している。
詳しくは、[クレーム・フロンジパーヌ](#creme-frangipane)の注を参照。

\atoaki{}



#### さくらんぼ・クラレット  {#cerises-au-claret}


<div class="frsubenv">Cerises au Claret</div>

大きめのさくらんぼを選び、ヘタの先端を切り落とし、銀製の深さのある型に並べる。
シナモン少々と砂糖を加えたボルドー産のワインを、さくらんぼがちょうどかぶる位の十分な量を上から注ぎ入れる。
型に\ruby{蓋}{ふた}をし、火のそばに 10 分間置いて、さくらんぼを[ポシェ\*](#pocher-gls)する。

そのままワインのシロップの中で冷ましたら、水気を切り、シロップを $\frac{1}{3}$ 量煮詰めて、
煮詰めたシロップ大さじ 6 杯あたり[グロゼイユのジュレ](#gelee-de-groseilles-a)を大さじ 1 杯加えて、
軽く[リエ\*](#lier-gls)する。

さくらんぼはよく冷やし、[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を別添で提供する


\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}いちじく {#figues-entremets-froids}

生を使うにせよ、加熱したものを使うにせよ、新鮮ないちじくは、下記のようないくつかの素晴らしいオントルメの材料になる。

<div class="recette">

#### 生のいちじくのカルルトン  {#figuse-a-la-carlton-crues}


<div class="frsubenv">Figues à la Carlton crues</div>

よく熟れた新鮮ないちじくの皮を\ruby{剥}{む}き、2 つに切り分けて深さのある型に並べ、砕いた氷で型を囲む。

別で、砂糖を加えたフランボワーズのピュレを氷の上で冷やす。
提供直前に、フランボワーズのピュレに、その 2 倍量の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を加え混ぜて、
その混ぜた物で、いちじくを完全に\ruby{覆}{おお}う。


\atoaki{}


#### 加熱したいちじくのカルルトン  {#figuse-a-la-carlton-cuites}


<div class="frsubenv">Figues à la Carlton cuites</div>

皮を\ruby{剥}{む}いた新鮮ないちじくを、バニラ風味のシロップで加熱しそのまま冷ます。
冷めたら水気を切り、深さのある型に並べ、[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を
加えたフランボワーズのピュレで、上記と同じよう\ruby{覆}{おお}う。


\atoaki{}


#### いちじくとクリーム  {#figuse-a-la-creme}


<div class="frsubenv">Figues à la Crème</div>

いちじくの皮を\ruby{剥}{む}き、2 つに切り分けて深さのある型に並べる。
ごく少量砂糖をふり、フルーツの\ruby{種}{たね}で風味付けしたリキュール大さじ数杯で
[アロゼ\*](#arroser-gls)して、砕いた氷で型を囲む。

提供直前に、バニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)で\ruby{覆}{おお}う。

###### 【原注】 {#nota-figuse-a-la-creme}

新鮮ないちじくを加熱してコンポートにして、[米](#riz-pour-entremets)やスムール、[フロンジパーヌ](#creme-frangipane)などを下に敷いても良い。

乾燥いちじくを同じようにバニラ風味のシロップか、赤ワインで加熱して、リ・オ・レ[^20]やスムールなどと提供しても良い。

新鮮ないちじくを、底に大さじ数杯の水を入れた素焼きの陶器の皿に置いて、砂糖を振りかけてからオーヴンで加熱しても良い。

個人的な意見では、オーヴンで加熱する方がシロップで加熱するよりも難しい。

[^20]: riz au lait、茹でた米の水を切り、牛乳、砂糖、塩、シナモンまたはバニラを加えて煮て、バターと卵黄を混ぜたデザート。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}いちご {#fraises-entremets-froids}

<div class="recette">

#### いちご・カルディナル  {#fraises-cardinal}


<div class="frsubenv">Fraises Cardinal</div>

冷えた形のきれいないちごを深さのある型に盛り、ソース・メルバ[^21]か新鮮なフランボワーズのピュレを塗って、
生のアーモンドとピスタチオを細長く切って散らす。

[^21]: sauce Melba、ブラックベリーやフランボワーズなどを使ったソース。


\atoaki{}


#### いちご・クレオル  {#fraises-a-la-creole}


<div class="frsubenv">Fraises à la Créole</div>

いちごとその同量の角切りのパイナップルを粉糖とキルシュで[マセレ\*](#macerer-gls)する。

均等にスライスしたパイナップルをキルシュで[マセレ\*](#macerer-gls)して、ずれないようにリング状に重ね、
脚付きの盛り付け台に載せる。

パイナップルのリングの中央に、いちごと角切りのパイナップルをピラミッド状に盛り、
キルシュ風味のシロップで[アロゼ\*](#arroser-gls)する。


\atoaki{}


#### いちご・フェミナ  {#fraises-femina}


<div class="frsubenv">Fraises Fémina</div>

形のきれいないちごを選び、砂糖をふってグラン・マルニエのキュラソー[^22]で[アロゼ\*](#arroser-gls)し、
そのまま氷の上で 1 時間[マセレ\*](#macerer-gls)しておく。

提供直前に、脚付きのグラスか深さのある型の底に、[マセレ\*](#macerer-gls)するのに使ったリキュールを加えた
[オレンジのアイス](#glace-a-l-orange)を敷く。

このアイスの上にいちごを並べる。

[^22]: curaçao Grand-Marnier、オレンジの皮で風味を付けたリキュールのキュラソーの内の 1 種で、
Grand-Marnier（グラン・マルニエ）はオレンジの皮で風味を付けた蒸留酒とコニャックなどのブランデーを熟成させた
リキュールの商標名。


\atoaki{}


#### いちご・レリナ  {#fraises-lerina}


<div class="frsubenv">Fraises Lérina</div>

カルメ産の黒メロン[^24]の、中央にある茎の部分を丸く切り離し、最初に種と繊維を取る。
次に、果肉をデザートスプーン等を使って取り、砂糖をふる。

好みの量のフレーズ・デ・ボワか、無ければ季節の物を、レリナのリキュールで[マセレ\*](#macerer-gls)する。

果肉を取ったメロンに、その果肉とフレーズ・デ・ボワを詰め、切り離した部分を戻して
\ruby{蓋}{ふた}をし、塩を加えてない氷を入れた容器の中で 2 時間冷やす。

最終的にナフキンの上に盛る。


[^24]: 保留中

\atoaki{}


#### いちご・マルゲリート  {#fraises-marguerite}


<div class="frsubenv">Fraises Marguerite</div>

小さめのフレーズ・デ・ボワを砂糖とキルシュで[マセレ\*](#macerer-gls)する。
次に、水気を切り、同量のザクロの[ソルベ](#sorbets-divers)で[リエ\*](#lier-gls)して、
前もってよく冷やしておいた銀製の深さのある型に盛る。
マラスキーノ酒風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)で、
全体を\ruby{覆}{おお}って、同じクリームで飾り付ける。

\atoaki{}


#### いちご・マルキーズ  {#fraises-marquise}


<div class="frsubenv">Fraises Marquise</div>

[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)にその半量のフレーズ・デ・ボワのピュレを加えて、
よく冷やした深さのある型に盛る。
大きめのと中くらいの綺麗な形のいちごを選んで、キルシュで[マセレ\*](#macerer-gls)し、
最後にグラニュ糖をまぶして、クリーム全体を\ruby{覆}{おお}うように並べる。

\atoaki{}


#### いちご・メルバ  {#fraises-melba}


<div class="frsubenv">Fraises Melba</div>

深さのある型の底に[バニラアイス](#glace-a-la-vanille)を入れる。
このアイスの上に、選別したいちごを一面に置き、砂糖を少量加えた濃いめのフランボワーズのピュレで\ruby{覆}{おお}う。

\atoaki{}


#### いちご・モンテ・カルロ  {#fraises-monte-carlo}


<div class="frsubenv">Fraises Monte-Carlo</div>

身の締まった綺麗な形のいちごを選び、深さのある型に入れ、砂糖をふってホワイトキュラソーで[アロゼ\*](#arroser-gls)し、
涼しい場所に置いておく。
少し形の良くないいちごで、\ruby{滑}{なめ}らかなピュレを作り、その $\frac{1}{3}$ 量のバニラ風味の
[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を加える。

客が来る前に、キュラソーのムースをドーム状の型に詰めて[グラセ\*](#glacer-gls)しておき、
同様に貝殻状のメレンゲも作っておく。

このメレンゲの貝殻を、可愛らしい小さな巣の形にした糸状の飴の中に置く。

盛り付け方。
ムースを型から外して楕円の皿に置き、飴の巣を皿の縁に並べる。
それぞれの飴の巣の底にクリーム入りのいちごのピュレを大さじ 1 杯入れ、
その上にキュラソーで[アロゼ\*](#arroser-gls)しておいたいちごを 3、4 個置く。

ドーム状のムースの表面にクリーム入りのいちごのピュレを塗り、
薄いベールのようにした糸状の飴で\ruby{蓋}{ふた}をしてモンテ・カルロ産の紫色のプラランを散らす。

\atoaki{}

#### いちご・ニナ  {#fraises-nina}


<div class="frsubenv">Fraises Nina</div>

[いちご・マルゲリート](#fraises-marguerite)のように作り、パイナップルの[ソルベ](#sorbets-divers)で[リエ\*](#lier-gls)する。

深さのある型に同じように盛り、砂糖入りのフランボワーズのピュレを加えた[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)で\ruby{覆}{おお}う。

\atoaki{}


#### いちご・レーヴ・ド・ベべ  {#fraises-reve-de-bebe}


<div class="frsubenv">Fraises Rêve de Bébé</div>

よく熟れた中くらいのパイナップルを選び、上の部分を切って皮を傷つけないように中身を取る。

厚さ約 2 cm の四角い[ジェノワーズ生地](#pate-a-genoise-ordinaire)の土台を用意して、
中身を取ったパイナップルを垂直に置けるように、中央の部分を少し\ruby{窪}{くぼ}ませる。
この[ジェノワーズ生地](#pate-a-genoise-ordinaire)の土台と同じ厚さと形にして、
乾燥焼きした生地に載せて貼り付ける。
ピンク色のフォンダンで[グラセ\*](#glacer-gls)し、グラス・ロワイヤル[^25]で飾り付けて
それぞれの\ruby{角}{かど}に大きめのいちごを 1 個ずつ置く。

取り出したパイナップルの中身の半量を薄切りにし、砂糖とキルシュ、マラスキーノ酒で[マセレ\*](#macerer-gls)する。

残りの半分の果肉をすりつぶして絞って果汁を取る。

パイナップルの皮の $\frac{3}{4}$ まで詰められるだけの十分な量のフレーズ・デ・ボワを、
パイナップルの果汁の中で[マセレ\*](#macerer-gls)する。

提供直前に、キルシュ風味のパイナップルと、フレーズ・デ・ボワを層状に交互に重ね、
それぞれの層の間にバニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)の層を挟みながら、
パイナップルの皮に詰める。

よく冷やして提供する。

[^25]: glace royale、卵白を泡立てながら粉糖とレモン汁を加えて作ったアイシング。

\atoaki{}


#### いちご・リッツ風  {#fraises-a-la-ritz}


<div class="frsubenv">Fraises à la Ritz</div>

いちごにたっぷり砂糖をふり、冷やして深さのある型に盛って、以下の[アパレイユ\*](#appareil-gls)で\ruby{覆}{おお}う。

フレーズ・デ・ボワ 250 g を網などで裏ごし、そのピュレにソース・メルバを少量加えてきれいなピンク色にし、
同量のよく泡立てたバニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を混ぜる。

提供する前によく冷やす。


\atoaki{}


#### いちご・ロマノフ  {#fraises-romanoff}


<div class="frsubenv">Fraises Romanoff</div>

形のきれいないちごをオレンジジュースとキュラソーで[マセレ\*](#macerer-gls)する。
冷やしておいた深さのある型に盛り、ぎざ模様の付いた口金を付けた絞り袋を使って
[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)で\ruby{覆}{おお}う。

\atoaki{}


#### いちご・サラ・ベルナール  {#fraises-sarah-bernhardt}


<div class="frsubenv">Fraises Sarah-Bernhardt</div>

形のきれいないちごを選び、キュラソーとコニャックで[マセレ\*](#macerer-gls)する。

提供直前に、深さのある型に[パイナップルのアイス](#glace-a-l-ananas)を敷いて、
いちごを載せキュラソーのムースで\ruby{覆}{おお}う。


\atoaki{}


#### いちご・ヴィルエルミーヌ  {#fraises-wilhelmine}


<div class="frsubenv">Fraises Wilhelmine</div>

大きくて形のきれいないちごを粉糖とオレンジジュース、キルシュで[マセレ\*](#macerer-gls)する。

深さのある型に盛り、バニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を別添で提供する。


\atoaki{}

#### いちご・ゼルマ・クンツ  {#fraises-zelma-kuntz}


<div class="frsubenv">Fraises Zelma Kuntz</div>

形のきれいないちごを冷やして深さのある型に盛る。
フランボワーズのピュレに同量の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を加えて、
いちごを\ruby{覆}{おお}う。

コルネ等で[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を絞って飾り付け、
粉状にしたヘーゼルナッツのプラランをまぶす。

\atoaki{}


</div> <!--endRecette-->

### \hspace*{-1zw}グーズベリー {#goosberries-entremets-froids}

<div class="recette">

#### グーズベリー・フール  {#goosberries-fool}


<div class="frsubenv">Goosberries fool</div>

小さめの緑グロゼイユ 500 g を軽めのシロップで[ポシェ\*](#pocher-gls)する。
火が入ったら、完全に水気を拭いて、網などで裏ごし、鍋の中にそのピュレを集める。

必要な量の粉糖をピュレに加えて氷の上でしっかりと混ぜる。
必要な量は、フルーツの酸味の具合と[ポシェ\*](#pocher-gls)に使ったシロップの濃さで変わる。

ピュレに、同量のしっかり[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)を混ぜ、
この[アパレイユ\*](#appareil-gls)を深さのある型にドーム状に盛って、
コルネで[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を表面に絞って飾り、
よく冷やして提供する。

###### 【原注】 {#nota-goosberries-fool}

元々イギリスでは、このオントルメでフルーツの裏ごしは必要ない。
フルーツを加熱したあとに、激しく泡立てるようにして潰すだけで十分だ。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}マンダリンオレンジ {#mandarines-entremets-froids}

<div class="recette">

#### マンダリンオレンジ・アルミナ  {#mandarines-almina}

<div class="frsubenv">Mandarines Almina</div>

直径 2 cm の無地の丸い抜き型で、マンダリンオレンジの枝が付いている部分の皮を丸く切り抜く。
次に中身を取り、[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を細かく砕いてマラスキーノ酒を染み込ませ
、紫色のモスコヴィットの[アパレイユ\*](#appareil-gls)に加えて、皮に詰める。

提供直前に、切り抜いた丸い皮で\ruby{蓋}{ふた}をしてナフキンに盛る。


\atoaki{}


#### マンダリンオレンジとクリーム  {#mandarines-a-la-creme}


<div class="frsubenv">Mandarines à la Crème</div>

マンダリンオレンジの中身を取り、少し固めのマンダリンオレンジ風味の[モスコヴィット](#moscovite-divers-aux-fruits)の
[アパレイユ\*](#appareil-gls)に、その $\frac{1}{3}$ 量の新鮮な生クリームを加えて、マンダリンの皮に詰める。

提供するまで冷蔵庫に入れておく。

\atoaki{}


#### マンダリンオレンジ・シュルプリーズ  {#mandarines-en-surprise}


<div class="frsubenv">Mandarines en Surprise</div>

[オレンジ・シュルプリーズ](#oranges-en-surprise)の、[オレンジのアイス](#glace-a-l-orange)を
マンダリンオレンジのジュレに代えて、同じように作る。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}メロン {#melons-entremets-froids}

<div class="recette">

#### メロン・フラッペ  {#melon-frappe}


<div class="frsubenv">Melon frappé</div>

よく熟れた中くらいのメロンを 2 個使う。
1 個目の皮と種、繊維を全て取り除き、果肉を網などで裏ごして[グラニテ](#granites)を作る。（アイスの章を参照）

2 個目のメロンの枝の周りに切り込みを入れ皮を取り、銀製のスプーンで種と繊維を完全に取り除き、
果肉をある程度の大きさの塊りのまま取って、砂糖少量とポルト酒、キュラソー、ラム酒、マラスキーノ酒などの
何かしらのワインかリキュールで、氷の上で[マセレ\*](#macerer-gls)する。

中身を取った皮を氷を入れた容器の中に 1 時間入れておく。

提供直前に、好みの形に削った小さめの氷の塊の上にメロンの皮を載せ、[グラニテ](#granites)と
[マセレ\*](#macerer-gls)しておいた果肉を、それぞれ層状に交互に重ねて詰める。

メロンに詰めたら、切り取ったメロンの枝の周りの部分を戻して\ruby{蓋}{ふた}をする。

###### 【原注】 {#nota-melon-frappe}

このメロンは、スプーンでよく冷やしておいた皿にすくって提供し、夕食が終わるまでたびたび氷の上に戻す。

\atoaki{}


#### メロン・オリエンタル  {#melon-a-l-orientale}


<div class="frsubenv">Melon à l’Orientale</div>

ちょうど良い柔らかさのメロンの、枝の周りに丸く切り込みを入れ、塊のまま取る。

種と繊維を取り除き、銀製のスプーンで果肉を取って大きめの角切りにする。
メロンの内側にたっぷりと粉糖をふり、フレーズ・デ・ボワと角切りのメロンを交互に層状に重ね、各層に砂糖を振りながら、
皮一杯に詰める。

キルシュ 1 dL を入れて仕上げ、切り抜いた皮で\ruby{蓋}{ふた}をして、帯状にしたバターで切れ目を埋め、
涼しい所に 2 時間置いてナフキンの上に盛る。

ゴーフレットと一緒に提供する。


\atoaki{}

#### メロン・シュルプリーズ  {#melon-en-surprise}


<div class="frsubenv">Melon en Surprise</div>

上記と同じようにメロンの中身を取り、その果肉を角切りにして新鮮な[フルーツのマセドワーヌ](#macedoine-de-fruits-rafraichis)に加え、
砂糖を加えたキルシュ風味のフレーズ・デ・ボワのピュレで[リエ\*](#lier-gls)したものを、メロンに詰める。

メロンに\ruby{蓋}{ふた}をして、2 時間冷蔵庫に入れておく。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}オレンジ {#oranges-entremets-froids}

<div class="recette">

#### オレンジのブロン・モンジェ  {#oranges-au-blamc-manger}


<div class="frsubenv">Oranges au Blanc-Manger</div>

マンダリンオレンジに書いてあるように皮に切り込みを入れて中身を取る。
次に[ブロン・モンジェ・フロンセーズ](#blanc-manger-a-la-française)の
[アパレイユ\*](#appareil-gls)を一杯に詰めて、そのまま置いておく。

切り取った皮の部分を戻して\ruby{蓋}{ふた}をし、ナフキンの上に盛る。

\atoaki{}

#### オレンジ・リュバネ  {#oranges-rubannees}


<div class="frsubenv">Oranges Rubannées</div>

数種類の色や香りを付けたブロン・モンジェの[アパレイユ\*](#appareil-gls)かまたは、
フルーツのジュレを、中身を取った皮に、交互に均等な層にして重ねて詰める。

提供直前に、\ruby{櫛}{くし}切りにする。

###### 【原注】 {#nota-oranges-rubannees}

\ruby{櫛}{くし}切りのオレンジ・リュバネは冷製オントルメの付け合わせとして提供することもよくある。


\atoaki{}

#### オレンジ・シュルプリーズ  {#oranges-en-surprise}


<div class="frsubenv">Oranges en Surprise</div>

オレンジの $\frac{3}{4}$ の高さの所を水平に切り、中身を取る。

[オレンジのアイス](#glace-a-l-orange)を皮に入れ、イタリアンメレンゲでこのアイスを\ruby{覆}{おお}って、
砕いた氷を敷いた天板に並べ、短時間でメレンゲに焼き色を付けるのに十分に熱くしたオーヴンに入れる。

オレンジをオーヴンから出したら、切り取った皮を戻し、それぞれの皮の上に飴で作った枝と葉を付ける。

ナフキンに盛る。

\atoaki{}


</div> <!--endRecette-->

### \hspace*{-1zw}ペッシュとネクタリン、ブルニョン {#peches-et-nectarines-ou-brugnons-entremets-froids}

ネクタリンとブルニョンはペッシュと同じ扱い方で良いので、分けて説明してペッシュと同じ作り方を繰り返す必要はないだろう。

<div class="recette">

#### ペッシュ・オドリエンヌ  {#peches-adrienne}


<div class="frsubenv">Pêches Adrienne</div>

来客時に、ペッシュが適度な柔らかさで同じように良い状態になるように選ぶ。
前述してあるように、熱湯に通して皮を\ruby{剥}{む}き、皿に並べて砂糖をふって涼しい場所に置いておく。

生クリームとフレーズ・デ・ボワとバニラ風味の滑らかなアイスと貝殻状のメレンゲを作って、メレンゲにペッシュを置く。

クリスタル製のグラスに、アイスを敷いて土台のようにして、その上にメレンゲの貝殻をはめ込む。
それぞれにペッシュを置いて、冷やしておいた何も加えてないキュラソーのムースも薄い層で\ruby{覆}{おお}う。
糸状の飴のベールをかぶせ、バラの花のクリスタリゼを散らす。

グラスを氷の塊にはめ込む。

\atoaki{}

#### ペッシュ・エリョン  {#peches-aiglon}


<div class="frsubenv">Pêches Aiglon</div>

ペッシュの皮を\ruby{剥}{む}き、バニラ風味のシロップで[ポシェ\*](#pocher-gls)して、
そのままシロップの中で冷ます。

冷めたら、水気を切り、砕いた氷を一杯に入れた二重底の銀製の深さのある容器に[バニラアイス](#glace-a-la-vanille)を層状に敷いて、その上にペッシュを盛る。

プラリネ・ヴィオレットをペッシュの上に散らし、\ruby{鷲}{わし}が上に乗っている岩のような形にした氷に
銀製の容器を盛って、氷ごと容器に糸状の飴のベールをかぶせる。

\atoaki{}


#### ペッシュ・ローロル  {#peches-a-l-aurore}


<div class="frsubenv">Pêches à l’Aurore</div>

ペッシェの皮を\ruby{剥}{む}き、キルシュ風味のシロップで[ポシェ\*](#pocher-gls)してそのままシロップの中で冷ます。

水気を切り、銀製の深さのある容器に[いちごのムース・グラセ](#composition-de-mousse-glacee-aux-fruits)
を敷いてペッシュを上に盛り、キュラソー風味の冷たい[サバイヨン](#sabayon)を表面に塗る。

\atoaki{}


#### ペッシュ・アレクサンドラ  {#peches-alexandra}


<div class="frsubenv">Pêches Alexandra</div>

バニラ風味のシロップで[ポシェ\*](#pocher-gls)してそのまま冷やしておく。

深さのある型の底に[バニラアイス](#glace-a-la-vanille)を敷いて、
表面をいちごのピュレで覆ってよく冷やしておき、その上にペッシュを盛る。
ペッシュの上に赤と白いバラの花びらを散らし、糸状の飴のベールをかぶせる。

\atoaki{}


#### ペッシュ・カルディナル  {#peches-cardinal}


<div class="frsubenv">Pêches Cardinal</div>

ペッシュをバニラ風味のシロップで[ポシェ\*](#pocher-gls)する。
しっかり冷えたら、深さのある容器に敷いておいた[バニラアイス](#glace-a-la-vanille)の上に盛る。
砂糖を加えたキルシュ風味の真っ赤なフランボワーズのピュレで\ruby{覆}{おお}い、
表面に細切りにした真っ白なアーモンドを散らす。

\atoaki{}

#### ペッシュ・シャトー・ラフィット風味 {#peches-au-chateau-laffitte}


<div class="frsubenv">Pêches au Château-Laffitte</div>

ペッシュを湯\ruby{剥}{む}きし、2 つに切る。
ペッシュを浸すのに十分な量のシャトー・ラフィットのワインに、
1 本あたり砂糖 300 g を加えて[ポシェ\*](#pocher-gls)する。

ワインのシロップの中に入れたまま冷やし、銀製の深さのある容器に盛る。
ワインの $\frac{3}{4}$ を煮詰め、少量のフランボワーズ入りのグロゼイユのジュレで[リエ\*](#lier-gls)する。

\atoaki{}


#### ペッシュ・ダム・ブロンシュ {#peches-dame-blanche}


<div class="frsubenv">Pêches Dame-Blanche</div>

バニラ風味のシロップで[ポシェ\*](#pocher-gls)する。
ペッシュが冷えたら、[バニラアイス](#glace-a-la-vanille)を敷いた深さのある容器に盛り、
キルシュとマラスキーノ酒で[マセレ\*](#macerer-gls)しておいたパイナップルの薄切りで\ruby{覆}{おお}う。
各ペッシュの間と\ruby{隙間}{すきま}全体に、ギザ模様の口金を付けた絞り袋で[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を大きめの点に絞って置く。

\atoaki{}


#### ペッシュ・ウージェニ {#peches-eugenie}


<div class="frsubenv">Pêches Eugénie</div>

ほどよい柔らかさのペッシュを選ぶ。
丁寧に種を取り皮を\ruby{剥}{む}いて、フレーズ・デ・ボワを差し込んだペッシュを、深さのある容器入れる。
キルシュとマラスキーノ酒大さじ数杯で[アロゼ\*](#arroser-gls)し、\ruby{蓋}{ふた}をして、前もって 1 時間氷の上に置いておく。

提供直前に、シャンパーニュ風味のとても冷たい[サバイヨン](#sabayon)をペッシュの表面全体に塗る。

\atoaki{}


#### ペッシュ・ランペラトリス {#peches-a-l-imperatrice}


<div class="frsubenv">Pêches à l’Impératrice</div>

ペッシュを 2 つに切り分け、バニラ風味のシロップで[ポシェ\*](#pocher-gls)してそのまま冷ます。
冷めたら、水気を切ってぬぐい、それぞれの半分のペッシュの\ruby{窪}{くぼ}みに、
丸ごとのペッシュの大きさの形に戻すのに十分な量の[バニラアイス](#glace-a-la-vanille)を詰める。
半分のペッシュの切り口の面に、固めの[アプリコットソース](#sauce-a-l-abricot)を塗り、
元の形に戻して、細切りにしたアーモンドのプラランを全体にまぶす。

フランボワーズ風味に[グラセ\*](#glacer-gls)した、乾燥焼きにした生地を底に敷いて、
キルシュとマラスキーノ酒に浸した[ジェノワーズ生地](#pate-a-genoise-ordinaire)をその上に載せた土台に、
ペッシュを盛る。

糸状の飴のベールをかぶせる。


\atoaki{}


#### ペッシュ・イザベル {#peches-isabelle}


<div class="frsubenv">Pêches Isabelle</div>

ほどよい柔らかさの大きめのペッシュ・ド・ヴィーニュの皮を\ruby{剥}{む}き、種を取って 2 つに切る。
深さのある容器に半割りのペッシュを並べて置き、砂糖をふって年代物のクロ・デ・パプで[アロゼ\*](#arroser-gls)する。

前もって 2 時間氷の上に置いておく。

バニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を別添で提供する。


\atoaki{}


#### ペッシュ・メルバ {#peches-melba}


<div class="frsubenv">Pêches Melba</div>

バニラ風味のシロップで[ポシェ\*](#pocher-gls)する。
[バニラアイス](#glace-a-la-vanille)を敷いた深さのある容器に盛り、フランボワーズのピュレを表面に塗る。

\atoaki{}


#### ペッシュ・ミストラル {#peches-mistral}


<div class="frsubenv">Pêches Mistral</div>

種から果肉が離れやすいくらいに適度によく熟れたペッシュの皮を\ruby{剥}{む}き、深さのある容器に並べて
砂糖をふり、なめらかないちごのピュレで\ruby{覆}{おお}う。
この上に、殻を\ruby{剥}{む}いた新鮮なアーモンドを並べる。
提供直前に、バニラ風味の「フルーレット」と呼ばれるクリーム[^30]でペッシュを\ruby{覆}{おお}う。

別で[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を添える。

[^30]: crème fleurette（クレーム・フルーレット）。泡立て用のクリーム。

\atoaki{}


#### ペッシュ・プティ・デュク {#peches-petit-duc}


<div class="frsubenv">Pêches Petit-Duc</div>

[ペッシュ・ダム・ブロンシュ](#peches-dame-blanche)と同じように作るが、クリームの点を絞らず、
代わりに、バール産の赤グロゼイユのコンフィチュールを置く。

\atoaki{}


#### ペッシュ・ロズ・シェリ {#peches-rose-cheri}


<div class="frsubenv">Pêches Rose-Chéri</div>

ペッシュをバニラ風味のシロップで[ポシェ\*](#pocher-gls)しそのまま冷やす。
[パイナップルのアイス](#glace-a-l-ananas)を線状に敷いた深さのある容器にペッシュを盛り、
シャンパーニュ風味のよく冷やした[サバイヨン](#sabayon)に、[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)
大さじ数杯を加えたものを全体に塗る。

表面に、砂糖の結晶をまぶしたバラの花びらを散らす。

\atoaki{}


#### ペッシュ・ロズ・ポンポン {#peches-rose-pompon}


<div class="frsubenv">Pêches Rose-Pompon</div>

大きめのペッシュの皮を湯\ruby{剥}{む}きし、バニラ風味のシロップで[ポシェ\*](#pocher-gls)してそのまま冷ます。

果肉を壊さないよう丁寧に、あまり切り口を広げすぎないように種を取り、種の代わりに[グラセ\*](#glacer-gls)したクルミを 1 粒入れる。

ペッシュを元の形に戻し、[バニラ](#glace-a-la-vanille)と[フランボワーズのアイス](#glace-aux-framboises)を
層状に敷いた銀製の深さのある容器に盛って、プララン風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)
で\ruby{覆}{おお}い、提供する $\frac{1}{2}$時間前に冷蔵庫に入れておく。

糸状のピンク色の飴のベールを容器にかぶせて仕上げる。

\atoaki{}


#### ペッシュ・サルタナ {#peches-sultane}


<div class="frsubenv">Pêches Sultane</div>

ペッシュをバニラ風味のシロップで[ポシェ\*](#pocher-gls)し、そのまま冷ます。

[ピスタチオのアイス](#glace-aux-pistaches)を敷いた深さのある容器にペッシュを盛り、
[リエ\*](#lier-gls)したシロップをよく冷やし、バラのエッセンスで香り付けたものを表面に塗る。
糸状の飴のベールをかぶせて、容器を氷の塊の上に盛る。

\atoaki{}


#### ペッシュ・トリアノン {#peches-trianon}


<div class="frsubenv">Pêches Trianon</div>

よく熟した「シャルム・ド・ベニュ」[^31]というペッシュを選んで、皮をむき砂糖をふる。

フルーツの種で風味付けしたリキュールに浸しておいたマカロン[^32]の塊を加えたバニラ風味のムース
をリング状に置いて、その上にペッシュを並べる。
フレーズ・デ・ボワのピュレを薄く表面全体に塗る。

[^31]: Charmes de Vénus（シャルメ・ド・ヴェニュ）、以下保留。

[^32]: 現代でよく見られる柔らかいタイプのマカロンではなく、
卵白、アーモンド、砂糖を混ぜて焼いた固いタイプのマカロン。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}洋梨 {#poires-entremets-froids}

<div class="recette">

#### 洋梨・アルマ {#poires-alma}


<div class="frsubenv">Poires Alma</div>

水 1 L、ポルト酒 2 $\frac{1}{2}$ dL、砂糖 250 g 、
[ブロンシール\*](#blanchir-gls)して[アシェ\*](#hacher-gls)したオレンジの[ゼスト\*](#zeste-gls)で香り付けた軽めシロップで、
皮をむいた洋梨を[ポシェ\*](#pocher-gls)する。

冷やして、深さのある容器に盛り、粉状のプラランをふって、一緒に[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を提供する。


\atoaki{}

#### 洋梨・カルディナル {#poires-cardinal}


<div class="frsubenv">Poires Cardinal</div>

洋梨をバニラ風味のシロップで[ポシェ\*](#pocher-gls)して、あとは[この名前のペッシュ](#peches-cardinal)と同じように作る。


\atoaki{}

#### 洋梨・フェリシア {#poires-felicia}


<div class="frsubenv">Poires Félicia</div>

熟れた洋梨を\ruby{櫛}{くし}切りにし、バニラ風味のシロップで[ポシェ\*](#pocher-gls)してそのまま冷ます。

小ぶりの洋梨の半割りを、同じようにピンク色のシロップで加熱する。

提供用の皿にリング状に置いた[クレーム・ヴィエノワーズ](#creme-moulee-a-la-viennoise)の中央に、
\ruby{櫛}{くし}切りの洋梨を盛る。バニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を
ピラミッド状に盛りながら洋梨を\ruby{覆}{おお}って、表面に赤いプララン[^32]を砕いて散らす。

クリームのリングの周りをピンク色の半割りの洋梨で囲む。

[^32]: pralines rouges、以下保留中
\atoaki{}

#### 洋梨・フロロンタン {#poires-florentine}


<div class="frsubenv">Poires Florentine</div>

油を塗った高さのある型に、スムールのモスコヴィットの[アパレイユ\*](#appareil-gls)をいっぱいに入れて、そのまま置いておく。

型から外したら、バニラ風味のアプリコットのピュレで[リエ\*](#lier-gls)した[洋梨のコンポート](#compote-de-poires)を置く。

\atoaki{}

#### 洋梨・エレーヌ {#poires-helene}


<div class="frsubenv">Poires Hélène</div>

洋梨をバニラ風味のシロップで[ポシェ\*](#pocher-gls)し、そのまま冷ます。
提供直前に、[バニラアイス](#glace-a-la-vanille)を層状に敷いて紫のプラランを散りばめた、深さのある容器に洋梨を盛る。

温かい[ソース・ショコラ](#sauce-au-chocolat)を別添で供する。

\atoaki{}

#### 洋梨・マリエット {#poires-mariette}


<div class="frsubenv">Poires Mariette</div>

小さめの洋梨を掃除し、バニラ風味の軽いシロップで茹でる。
\ruby{滑}{なめ}らかなバニラ風味の[マロンのピュレ](#puree-de-marrons)を敷いた、
少し深さのある皿に盛り、年代物のラム酒を加えた煮詰めた[アプリコットソース](#sauce-a-l-abricot)を表面に塗る。

\atoaki{}

#### 洋梨・マルキーズ {#poires-marquise}


<div class="frsubenv">Poires Marquise</div>

洋梨をバニラ風味のシロップで茹で、水気を切って冷やす。
しっかり冷えたら、濃度のとても濃いフランボワーズ入りグロゼイユのジュレを、表面に何度か塗り重ね、
すぐにアーモンドのプラランの[アシェ\*](#hacher-gls)を散りばめる。

モンケ型で作って、型から外して丸い平皿に載せた[プディング・ディプロマット](#pudding-diplomate)の上に置く。
正確に小さな三角形に切り揃えた[りんごのジュレ](#gelee-de-pommes)を、リング状のクルトン全体に貼り付けて、プディングの底の周りを囲むように置く。

\atoaki{}

#### 洋梨・マリー・ガルデン {#poires-mary-garden}


<div class="frsubenv">Poires Mary Garden</div>

洋梨をシロップで茹でて冷まし、[さくらんぼのコンポート](#compote-de-cerises)を加えたソースメルバを敷いた、深さのある容器に盛る。

\atoaki{}

#### 洋梨・メルバ {#poires-malba}


<div class="frsubenv">Poires Melba</div>

洋梨をバニラ風味のシロップで[ポシェ\*](#pocher-gls)し、あとは[同じ名前のペッシュ](#peches-melba)と同じように作る。

\atoaki{}

#### 洋梨・プララン {#poires-pralinees}


<div class="frsubenv">Poires Pralinées</div>

[洋梨をコンポート](#compote-de-poires)にし、そのまま冷ます。

深さのある容器に盛り、生クリーム少量でのばした[クレーム・フロンジパーヌ](#creme-frangipane)を表面に塗る。

それぞれの洋梨の間に、しっかりと形を整えた大さじ 1 杯分の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を置いて、
粗めに刻んだアーモンドのプラランで全体を\ruby{覆}{おお}う

冷製か温製の[ソース・ショコラ](#sauce-au-chocolat)を一緒に供する。

\atoaki{}

#### 洋梨・レリジューズ {#poires-a-la-religieuse}


<div class="frsubenv">Poires à la Religieuse</div>

バニラ風味のシロップで[洋梨をコンポート](#compote-de-poires)にし、そのまま冷まして、
洋梨と同じくらいの高さの、深さのある磁器製の容器に盛る。
[チョコレートのモスコヴィット](#moscovites-divers-a-la-creme)の[アパレイユ\*](#appareil-gls)を
容器一杯に詰めて提供する前に 2 時間冷蔵庫に入れておく。

\atoaki{}

#### 洋梨・ラム酒風味 {#poires-au-rhum}


<div class="frsubenv">Poires au Rhum</div>

[洋梨をコンポート](#compote-de-poires)にし、深さのある容器に盛る。
シロップをアロウ・ルート[^36]で[リエ\*](#lier-gls)し、薄いピンク色にして、ラム酒で香り付け、洋梨に注ぎかけて冷ます。

###### 【原注】 {#nota-poires-au-rhum}

この洋梨は、提供直前に熱いラム酒を注いで、客にオントルメを見せる前に火をつける以外は、同じ作り方で、温製で提供しても良い。

[^36]: arrow-root

\atoaki{}

#### 洋梨・リシュリュー {#poires-richlieu}


<div class="frsubenv">Poires Richelieu</div>

[フラムリ](#flamri)の[アパレイユ\*](#appareil-gls)（その他の冷たいオントルメを参照）を、無地のリング状の型に詰め、フルーツの砂糖漬けで飾る。

[ポシェ\*](#pocher-gls)して型が冷めたら、型から外して丸い平皿に置く。

バニラ風味のシロップで\ruby{櫛}{くし}切りの[りんごをコンポート](#compote-de-pommes)にして中央にピラミッド状に置き、
[クレーム・フロンジパーヌ](#creme-frangipane)に、その $\frac{1}{4}$ 量の乾いたマカロンを細かく砕いたものと、
2 倍量のしっかり泡立てた[クレーム・シャンティ](#)加えたものを、りんごの表面に塗る。

コルネで[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を上に絞って飾り、別添でキルシュ風味の[アプリコットソース](#sauce-a-l-abricot)を供する。

\atoaki{}

</div> <!--endRecette-->


### \hspace*{-1zw}りんご {#pommes-entremets-froids}

<div class="recette">

#### りんご・フェリシア {#pommes-felicia}


<div class="frsubenv">Pommes Félicia</div>

りんごを\ruby{櫛}{くし}切りにし、バニラ風味のシロップで[ポシェ\*](#pocher-gls)して、
あとの作り方については[同じ名前の洋梨](#poires-felicia)と同じように作る。

\atoaki{}

#### りんご・ロワイヤル {#pommes-a-la-royale}


<div class="frsubenv">Pommes à la Royale</div>

小さめのりんごの皮をむき、円柱形のくりぬき器で芯の部分を取ってバニラ風味のシロップで[ポシェ\*](#pocher-gls)する。

しっかりと冷えたら、グロゼイユのジュレを表面に塗り、ブロン・モンジェのタルトレットに 1 個ずつ置いて、円形の平皿にリング状に並べる。

その中央に、マラスカン種のさくらんぼを[アシェ\*](#hacher-gls)して入れる。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}その他の冷たいオントルメの章

<div class="recette">

#### ビスキュイ・モンテ・カルロ {#biscuit-monte-carlo}


<div class="frsubenv">Biscuit Monte-Carlo</div>

少し湿らせた天板等に紙を敷いて、フラン用の大きめセルクルを 5 個置く。
ムラングをセルクルの半分の高さまで入れ、ムラングを焼いて乾燥機に 24 時間入れておいて、しっかりと乾燥させる。
このムラングの円盤を、削ったチョコレートをふった[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を、均等な量ずつ挟みながら重ねる。
最後の円盤の表面をチョコレートで[グラセ\*](#glacer-gls)し、[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)をコルネで絞って、
このビスキュイを\ruby{縁}{ふち}どる。

ビスキュイの\ruby{縁}{ふち}の上に、クレームを小さな\ruby{菱}{ひし}形にリング状に絞って、それぞれの\ruby{菱}{ひし}形の中に紫のプラランを 1 粒置く。


\atoaki{}



#### クルート・ヨアンヴィーユ {#croute-joinville}


<div class="frsubenv">Croûte Joinville</div>

輪切りにした柔らかい[サバラン生地](#pate-a-savarin)にキルシュ風味のシロップを軽く染み込ませ、
キルシュで[マセレ\*](#macerer-gls)した薄く輪切りにしたパイナップルと、交互にターバン状に盛って、
中央にバニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)をピラミッド状に入れ、チョコレートを削って散らす。

このターバン状のクルートをキルシュ風味のアプリコットのシロップで包む。

\atoaki{}



#### クルート・メキシコ風 {#croute-mexicaine}


<div class="frsubenv">Croûte Mexicaine</div>

固くなった[ジェノワーズ生地](#pate-a-genoise-ordinaire)を、長さ 6 cm、厚さ 8 mm に楕円形に切る。

コンデのプラランで一面\ruby{覆}{おお}って、低温のオーブンで乾かす。
このクルートを円形の平皿の上にリング状に置いて、その中央に[アイス・プロンビエール](#glace-plombiere)を岩のような形に盛る。

\atoaki{}

#### クルート・ノルマンド・シャンティ {#croute-normande-a-la-chantilly}


<div class="frsubenv">Croûte Normande à la Chantilly</div>

温かい[クルート・ノルマンド](#croute-a-la-normande)と同じようにクルートを作る。
ターバン状に盛り、りんごのマーマレードで[リエ\*](#lier-gls)したシロップを、
しっかりと冷やしておくこと以外は、温かいクルートと同じようにして染み込ませる。

\atoaki{}

#### フルーツのディプロマット {#diplomate-aux-fruits}


<div class="frsubenv">Diplomate aux fruits</div>

作り方。
1……フルーツ入りの[ジェノワーズ生地](#pate-a-genoise-ordinaire)の土台を、[フィレに加熱した](#cuisson-du-sucre)アプリコットで[グラセ\*](#glacer-gls)する。

2……フルーツ入りのバヴァロワを作る。

バヴァロワを型から外してジェノワーズの台の上に載せ、バヴァロワに使ったフルーツと同じ種類のフルーツの[コンポート](#compotes-simples)で囲む。

\atoaki{}

#### イートン・メス {#eton-mess}


<div class="frsubenv">Eaton Mess</div>

これは、ヘタをとったいちごと[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)を同量ずつボウルに入れて、フォークで混ぜただけの物だ。
盛り方は好みの問題だが、出来るだけ簡素にするべきだ。

\atoaki{}

#### フラムリ {#flamri}


<div class="frsubenv">flamri</div>

白ワインと水 $\frac{1}{2}$ L ずつを混ぜて沸かし、細かいスムール 250 g を振り入れる。
そのまま弱火で 20 分間茹でる。
その後、以下の[アパレイユ\*](#appareil-gls)を加える。
粉糖 300 g、塩 1 つまみ、全卵 2 個と泡立てた卵白 6 個分を混ぜる。
バターを塗った波模様のある型に注ぎ入れ、湯煎で[ポシェ\*](#pocher-gls)してそのまま冷ます。

型から外して、いちごやグロゼイユ、さくらんぼなどの、生のフルーツのピュレに適量の砂糖を加えたものを表面に塗る。


\atoaki{}

#### イル・フロットント {#ile-flottante}


<div class="frsubenv">Ile flottante</div>

固くなった[ビスキュイ・サヴォワ](#pate-a-biscuit-de-savoie)を、薄く輪切りにしてキルシュとマラスキーノ酒を染み込ませ、
[アプリコットのコンフィチュール](#confitures-d-abricots)を全体に塗って、コリント産のレーズンとアーモンドの[アシェ\*](#hacher-gls)をまぶす。

ビスキュイ同士を重ねて元の形に戻し、砂糖を加えたバニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を全体に塗る。

クレームの表面に、細切りのピスタチオとコリント産のレーズンをまぶし、コンポート用の深さのある容器に盛って、
バニラ風味の[クレーム・オングレーズ](#creme-a-l-anglaise-a)かフランボワーズのシロップを周りに流す。


\atoaki{}

#### ユンケ・ミルク {#junket-milk}


<div class="frsubenv">Junket Milk</div>

牛乳 1 L を軽く温める。35 ℃ になったら火から外し、好みの香り付けと砂糖 50 g、
レネット種のりんごのエッセンスを 6 滴か、またはその飴を水 6 滴に溶かして加える。
深さのある型に注ぎ、よく冷やして提供する。

###### 【原注】 {#nota-junket-milk}

このオントルメは、とても繊細かつ簡素で、牛乳に砂糖を入れ、香りを付け、熱とレネット種のりんごのエッセンスの 2 つの作用で固めるだけだ。

\atoaki{}

#### 冷たいフルーツのマセドワーヌ {#macedoine-de-fruits-rafraichis}


<div class="frsubenv">Macédoine de fruits rafraîchis</div>

よく熟した洋梨とペッシュ、皮をむいて薄切りにしたアプリコットとバナナ、などのように季節のフルーツを使う。
小さめか大きめのいちご、フランボワーズ、実だけにした白と赤のグロゼイユ、薄皮をむいた新鮮なアーモンドなどを加える。

これらのフルーツをよく冷やした深さのある容器に入れてしっかりと混ぜ、キルシュかマラスキーノ風味の 30 °Bé のシロップで
[アロゼ\*](#arroser-gls)してそのまま 1 時間または 2 時間、時々全体を混ぜながら[マセレ\*](#macerer-gls)する。

\atoaki{}

#### ウジェニア・クレーム・イタリエンヌ {#eugenia-creme-a-l-italienne}


<div class="frsubenv">Eugénia-Crème à l’Italienne</div>

よく熟れたウジェニア[^37]を選んで、皮をむき、薄切りにしてマラスキーノ酒風味のシロップで[マセレ\*](#macerer-gls)する。

[バニラアイス](#glace-a-la-vanille)を敷いた深さのある容器に盛って、
[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を上に絞って飾り、砂糖の結晶をまぶしたスミレの花を散らす。

[^37]: Eugénia、南米熱帯地方原産の、かぼちゃのような形のフルーツ。日本では沖縄などで外来種として着々と生育範囲を広げているが、日本全体としてはあまり一般的ではないフルーツ。熟すと赤くなり、生食のほか、ジュース、ジャム、アイスクリームなどに加工される。
果皮が薄く保管に向かない。

\atoaki{}

#### マルキーズ・アリス {#marquise-alice}


<div class="frsubenv">Marquise Alice</div>

アニゼット酒を染み込ませておいた[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を内側に貼ったモンケ型で、
プラリネのモスコヴィットを作る。

型から外して提供用の平皿に載せ、砂糖を加えたバニラ風味のしっかりと泡立てた[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を全体に均一に塗る。

グロゼイユのジュレをコルネで絞って、上に並行な線を描き、その後、ペティナイフ等の先端を使って、この線を横に切る。

小さめの三角形にした[フィユタージュ](#feuilletage)をコンデのプラランで覆ってオーブンで乾燥させ、菓子の底を囲む。

\atoaki{}

#### メレンゲ・ジェルマン {#meringues-germaine}


<div class="frsubenv">Meringues Germaine</div>

とても若い小さめのジュルヴェ[^38]のチーズ 3 個を、ボウルに入れて木製のスプーンで潰し、チーズと同量のクレーム・エペスと生クリーム、
粉糖大さじ 3 杯とフレーズ・デ・ボワか季節のフルーツのピュレ大さじ 3 杯を混ぜる。
[アパレイユ\*](#appareil-gls)が十分に鮮やかなピンク色になってなければ、植物性の赤い色素を数滴入れて色を足しても良い。

好みで、[アパレイユ\*](#appareil-gls)にバニラか、キルシュ、オレンジの[ゼスト\*](#zeste-gls)のすりおろしで香りを付けても良い。

しっかりと乾燥させた、きれいな貝殻の形のムラングの中に盛る。

[^38]:Gervais、保留中

\atoaki{}

#### いちごのモン・ブロン {#mont-blanc-aux-fraises}


<div class="frsubenv">Mont-Blanc aux fraises</div>

バニラ風味のシロップで[マセレ\*](#macerer-gls)して冷やし、しっかりと水気を切っておいたフレーズ・デ・ボワを、
砂糖を加えたしっかりと泡立てたバニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)に加える。
クレーム 1 L あたりフレーズ 125 g の割合で加える。

ドーム状に盛り、グラニュ糖をまぶした大きめのいちごを底の周りに置いて、大きめの真っ赤ないちごの半割りで表面を飾る。

\atoaki{}

#### マロンのモン・ブロン {#mont-blanc-aux-marrons}


<div class="frsubenv">Mont-Blanc aux marrons</div>

砂糖とバニラを入れた牛乳でマロンを茹で、無地の高さのある型の上に、裏返して置いた網でマロンを裏ごして、
網から落ちるピュレがバミセリのような形になり、自然と型に入るようにする。

型の外側に落ちたピュレも入れ終わったら、型から外して提供用の皿に載せ、
中央に砂糖を加えたバニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を、でこぼこになるように不規則に山のように盛る。

\atoaki{}

#### モン・ロズ {#mont-rose}


<div class="frsubenv">Mont-Rose</div>

凍らせおいた背の低いマドレーヌ型で[シャルロット・プロンビエル](#charlotte-plombiere)を作る。

型から外して提供用の皿に載せたら、新鮮なフランボワーズのピュレを加えた[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を
スプーンでピラミッド状の岩のような形にしながら、シャルロットを\ruby{覆}{おお}う。

\atoaki{}

#### ムース・モンテカルロ {#mousse-monte-carlo}


<div class="frsubenv">Mousse Monte-Carlo</div>

とても新鮮なクリーム  $\frac{1}{2}$ L をコシが出るまで泡立てる。
粉糖 80 g とバニラシュガー 20 g、乾燥焼きにしたムラング 50 g を小さな塊に砕いて、クリームと軽く混ぜる。

銀製の 2 重底の深さのある容器に盛り、砕いた氷を当てて 1 時間冷やす。

\atoaki{}

#### 卵のムースリーヌ・レジャヌ {#mousseline-d-oeufs-rejane}


<div class="frsubenv">Mousseline d’Œufs Réjane</div>

[標準的なムラング](#meringue-ordinaire)を、模様の付いてない丸い口金をつけた絞り袋で、
白い紙の上に、大きめのマカロンくらいの大きさに絞る。
砂糖とバニラを入れて沸かした牛乳の中に、この紙を\ruby{滑}{すべ}らせるように入れ、
ムラングが自然に\ruby{剥}{は}がれるようになったら紙を取る。
ムラングを[ポシェ\*](#pocher-gls)し終えたら、水気を切る。

磁器製か銀製の卵用の皿にムラングを 2 個盛り、それぞれのムラングの真ん中に、
[ポシェ\*](#pocher-gls)した半割りの大きめのアプリコットを 1 個置いて、
濃いめのクレーム・オングレーズ大さじ数杯を表面全体に塗る。

\atoaki{}

#### ウー・ア・ラ・ネージュ {#oeufs-a-la-neige}


<div class="frsubenv">Œufs à la neige</div>

[標準的なムラング](#meringue-ordinaire)をスプーンで卵のような形にし、砂糖とバニラを入れた牛乳を鍋で沸かしている所に、
成形したものを落としていく。
均等に火が入るようにこのムラングを牛乳の中で裏返して、しっかりと固くなったらすぐに網の上に引き上げて水気を切る。

牛乳をモスリンの布でこして、1 L あたり卵黄 10 個を加え、[クレーム・オングレーズ](#creme-a-l-anglaise-a)を作る。
ムラングをコンポート用の容器に盛り、[クレーム・オングレーズ](#creme-a-l-anglaise-a)を全体にかける。

しっかりと冷やして提供する。

\atoaki{}

#### ウー・ア・ラ・ネージュ・ムーレ {#oeufs-a-la-neige-moules}


<div class="frsubenv">Œufs à la neige moulés</div>

[ウー・ア・ラ・ネージュ](#oeufs-a-la-neige)と[クレーム・オングレーズ](#creme-a-l-anglaise-a)を上記と同じように作るが、
[クレーム・オングレーズ](#creme-a-l-anglaise-a)に冷水で戻しておいたゼラチン 5、6 枚を加える。

油を塗った、縁が高い型にムラングを並べ、固まらない程度にしっかりと冷やした[クレーム・オングレーズ](#creme-a-l-anglaise-a)で
\ruby{覆}{おお}って、涼しい場所に置くか、周りに氷を当てる。

\atoaki{}

#### 米・ランペラトリス {#riz-a-l-imperatrice}


<div class="frsubenv">Riz à l’Impératrice</div>

バニラ風味の[オントルメ用の米](#riz-pour-entremets)を、前述に通りの牛乳と砂糖の割合で作る。

米に火が入って少し冷めたら、生の米 250 g あたり、フルーツの砂糖漬けの[サルピコン](#salpicons-divers) 125 g と
アプリコットのピュレ大さじ 4 杯を加える。
そこに、上記の混ぜ物と同量のキルシュ風味のモスコヴィットの[アパレイユ\*](#appareil-gls)か、または[粘度を付けたクレーム・オングレーズ](#creme-a-l-anglaise-b) 5 dL と
[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly) 5 dL を混ぜたものを加える。

背の高い型の底にグロゼイユのジュレを層状に敷き、その上に米の[アパレイユ\*](#appareil-gls)を流して、涼しい所に置くか型に氷を当てる。

提供直前に型から外してナフキンに載せる。

\atoaki{}

#### 米・マルテーズ {#riz-a-la-maltaise}


<div class="frsubenv">Riz à la Maltaise</div>

上記と同じように米を用意するが、オレンジの[ゼスト\*](#zeste-gls)で香り付けし、
アプリコットのピュレとフルーツの砂糖漬けの[サルピコン](#salpicons-divers)は加えない。
同量の[オレンジのモスコヴィット](#moscovite-aux-fruits)の[アパレイユ\*](#appareil-gls)を混ぜ、
ドーム状の型に注ぎ入れて氷を当てる。

型から外して丸い平皿に載せたら、むき出しの\ruby{房}{ふさ}に分けただけのオレンジの果肉と、
その[ゼスト\*](#zeste-gls)で香り付けしたシロップで[マセレ\*](#macerer-gls)しておいた果肉を、
交互に並べながらドームを\ruby{覆}{おお}う。

\atoaki{}

#### ロッド・グロッド、デンマークのオントルメ {#rod-grod}


<div class="frsubenv">Rod Grôd</div>

銅製の鍋に赤グロゼイユ 500 g とフランボワーズ 250 g 、水 8 dL を入れる。
一度沸かして、目の細かい網で\ruby{漉}{こ}す。約 1 $\frac{1}{2}$ L の液体が取れるはずで、
それに砂糖 380 g、じゃがいもの澱粉 35 g とサゴウ澱粉 35 g（両方とも少量の水で溶いておく）、
赤ワイン 2 dL バニラビーンズ $\frac{1}{4}$ 本分を加える、
もう 1 度火にかけて、混ぜながら 2 分間沸かし続ける。

その後、軽く湿らせて粉糖をまぶすか、シロップを塗っただけの陶器の専用の型にこの[アパレイユ\*](#appareil-gls)を注ぎ入れる。
提供する前に 48 時間涼しい場所で休ませておく。

提供直前に型から外して提供用の皿に載せ、生クリーム少量と牛乳少量を一緒に提供する。

\atoaki{}

#### フルーツのスエドワーズ {#suedoise-de-fruits}


<div class="frsubenv">Suédoise de fruits</div>

[ジュレ](#gelees)の作り方の説明に書かれているように、フルーツのスエドワーズは[ジュレ](#gelees)の 1 つで、
可能な限りフルーツとその色合いを色々な種類用意して、その[コンポート](#compotes-simples)を層状に重ねながら
[アスピック](#aspics)用の型に入れて作る。


\atoaki{}

#### いちごのティボリ {#tivoli-aux-fraises}


<div class="frsubenv">Tivoli aux fraises</div>

中央に空洞があり縁に装飾の付いた型の内側を、同じ厚さのとても澄んだキルシュ風味のジュレの層で覆う。

フレーズ・デ・ボワのピュレをたっぷりと加えたモスコヴィットの[アパレイユ\*](#appareil-gls)を型一杯に入れ、そのまま置いておく。

型から外したらとても澄んだキルシュ風味のジュレの[アシェ\*](#hacher-gls)で周りを囲む。

\atoaki{}




</div> <!--endRecette-->
