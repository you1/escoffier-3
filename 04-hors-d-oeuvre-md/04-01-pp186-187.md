---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->


<!--IV.オードブル-->



# オードブル（冷製、温製）{#hors-d-oeuvre-froids-et-chauds}

<!--<div class="frchapenv">Hors-d'œuvre froids et chauds</div>-->


<!--\index{おーとふる@オードブル|(}-->
<!--\index{おるとううる@オルドゥーヴル|see{オードブル}}-->

<!--\normalfont\normalsize\setlength{\parindent}{1zw}\vspace{1zw}-->

## 概説[^8] {#observation-sur-hors-d-oeuvre}

[^8]: 初版には概説に相当する部分がない。第二版で追加された概説はその後の異同なし。

<!--\large\it

<div class="center">Observation</div>-->



オードブルという名称には、献立におけるその役割がはっきりと表われている[^5]。
要するにアクセサリー、欠くべからざるものとは真逆、その必要があればオー
ドブルを献立からなくしても全体の調和はしっかり保てる、というものだ。少
なくともディナーについてはそう言える。

つまりは、軽い品、繊細な組み合わせで作られるべきもので、どんな場合でも
一皿として完結するようなものであってはいけないということだ。だが、量が
少ないことは味のよさで補うべきものだし、繊細な盛り付けをするからこそ、
オードブルはパーフェクトなものになり得る。

オードブルには2種類ある。ひとつは冷製、もうひとつは温製だ。このふたつ
はサービス（給仕）の面でも調理の面でもまったく違うものと考えていい。




[^5]: 原文 hors-d'œuvre （オルドゥーヴル）< hors 〜外の、œuvre 仕事、
    作品、の合成語。つまり「仕事の範疇外のもの」=料理人の作品として見
    てもらいたいものではなく余技、飾り、といったニュアンスがある。料理
    においてこの語がもちいられるようになったのはおそらく17世紀。それ以
    前は建築用語として、規格外の、あるいは本館に対する別館、といった意
    味でもちいられていたようだ。フュルチエールという17世紀の学者、文人
    の死後出版となったフランス語辞典（1690年）では、hors の項目におい
    て「壮麗な宴席において供される、一般の宴席で供されるものを越えた料
    理」と定義し、œuvre の項では、「通常の献立のほかに食卓に供される料
    理」としている。また、19世紀のエミール・リトレによる辞書には、この
    語を「ラディッシュ、バター、アンチョビなどの皿。ポタージュの後、最
    初の料理（というのは、かつておもな料理が並べられる順に含まれていな
    かったからそういわれている）の前に供される。たくさんの種類がある」
    と定義し、用例として17世紀の文人ラブリュイエールの『カラクテール
    （人さまざま）』の一節……いわゆるグルマンについての小文(*Les
    Caractères*, in *Œuvres de la Bruyère*, Hachette, 1865, t.2,
    pp.56-58.）から「彼はオードブルも果物も小皿料理もどれも憶えていた」と
    引用している。18世紀後半に出版された『フランスのカナメリスト』
    （1768年）というサーヴィスの本では、or d'œuvre の綴りで「配膳室
    （office）で用意するメロン、無花果、フレッシュなバターと蕪、ごく小
    さな生食できる若獲りのアーティチョーク、これらは厨房でつくられる料
    理とともに供される」とある。この語がとりわけ意味を持つようになった
    のは19世紀にロシア式サーヴィスが普及して「コース」の概念が形成され
    ていった結果だろう。食卓にいちどに複数の料理を並べるフランス式サー
    ヴィスでは\kenten{おまけ}的なものであったオードブルが、料理を順に
    供するロシア式サーヴィスでは「前菜」の位置、こんにちでいうならア
    ミューズ的な位置付けがなされるようになったからだ。エスコフィエの時
    代はその過渡期にあったと考えていいだろう。また、ラディッシュとバター
    の組み合わせが手軽なオードブルの代表的なもののひとつとしてこんにち
    でもよく知られているが、すくなくとも18、19世紀にはポピュラーなもの
    だったことは注目に値する。なお、この hors-d'œuvre という複合語の単
    数形と複数形はおなじ。複合語の複数形は単語によってどの部分に sを付
    けるかどうかがまちまちなので、ひとつずつ覚えること。ex. chou-fleur
    （シュフルール、カリフラワー）> chou**x**-fleur**s**,
    agro-alimentation （アグロアリモンタシォン、食品工業）>
    agro-alimentation**s** など。




## 冷製オードブル[^9] {#hors-d-oeuvre-froids}

[^9]: この項も第二版から。その後はほぼ異同なし。

<!--<div class="frsecenv">Hors-d'œuvre froids</div>-->


\index{hors-d'oeuvre froids@hors-d'œuvre froids|(}
\index{おとふるれいせい@オードブル（冷製）|(}

<!--
\thispagestyle{empty}
-->

一般的にいって、冷製オードブルはポタージュを含まない献立にのみふさわし
い。これについては[「ポタージュを供することについ
て」](#consideration-sur-le-service-des-potages)ですでに述べたとおりだ。

とはいえ、この原則もアラカルト方式のレストランではあまり守られていない。

もっとも、こういう場合には大抵、キャビアや牡蠣、ヴァノーの卵といった豪
華なオードブルであって、魚料理やサラダ、野菜のマリネほどには、後に続い
て供される料理を味わうのに影響を与えない。注意しておきたいのは、ほとん
どの場合、冷製オードブルは注文をうけた料理を準備している間、お客様をお
待たせしないためにすぐ供されるものでしかないということだ。

冷製オードブルをこのように使うのもある程度までは許容できる。

ただ、冷製オードブルをこのようにして、どんな場合でも、必要ない場合でも
使い過ぎてしまっていることは残念でならない。

こんなふうに冷製オードブルが濫用されるに至ったのは、ある時期にロシアの
ものが爆発的に大流行したせいでもある[^2]。

[^2]: ナポレオンの第1帝政期とそれに続く王政復古期に、ロシアに招聘され
    た料理人たちが帰国してロシアの食文化を伝え、それが広まったのが7月
    王政期（1830〜1848）および第2帝政期（1848〜1878）にかけてのこと。
    とりわけ第2帝政期はロシア帝国の国力の増大と相まってロシア文化を真
    似ることは流行の先端だった時期でもある。


ロシアでは、食卓のある部屋の隣りに多くのパティスリや魚の燻製などのビュ
フェをしつらえるという習慣がある。とりあえず食卓につく前に、お客様はそ
れを立食し、強いアルコールで喉をうるおす。

食事のプロローグともいうべきこれらの料理はまとめて「ザクースキ」と呼ばれた。


    
    
食事会の主催者たちもメートルドテルたちもロジックなんか気にせず熱心に我
が国にこのザクースキの方式をとり入れた。気候の違いから我々フランス人と
ロシア人のあいだに味の好みや習慣、もちろん気温の違いがあることなんか頓
着しなかった。

じっさい、ごくわずかな例外を除いて、「ザクースキ」として供されたのは名
ばかりのもので、たんにごく普通の冷製オードブルでしかなかったし、それも
食卓のある部屋で供されさえした。もうこの時点で、ザクースキの約束事なん
かねじ曲げられてしまっていたわけだ。


が、幸いなことに、この（文化的とは言いがたい）外国語で、冷製オードブル
のような単純でありきたりなものを呼ぶことが馬鹿馬鹿しいと人々は気づいた
のだった。こんにちでは、献立にこのロシア語を見かけることはほとんどなく
なった。

我々としては、冷製オードブルをディナーに入れる理由がまったくわからない
し、理屈からいって、冷製オードブルをディナーに組み込むとポタージュの風
味を台無しにしてしまう結果しか考えられない。

せいぜいが、キャビアはよく冷やしたものにかぎるが、そのノワゼットのよう
な風味が口蓋にいい印象をあたえてくれるし、ある種の上等の生牡蠣[^3]はラ
インかボルドーのごく白い辛口のワイン[^4]の供としてであればいいだろう[^7]。
だが、どんな種類の、どんな調理の魚料理であっても、どんなサラダや野菜の
マリネであっても、大事なことだから繰り返し述べるが、ディナーの献立から
は絶対に冷製オードブルを外すべきなのだ。

[^3]: いわゆるブロン種など。

[^4]: こんにちでは生牡蠣とシャブリの組み合わせのほうが「定番」のイメー
    ジがあるが、すくなくともエスコフィエの時代はそうでなかったと考えら
    れる。そもそも生牡蠣と白ワインという組み合わせが絶対的に美味なもの
    であるかも、味覚というもの、あるいは食事というものが\kenten{個人的
    な体験}である以上は、絶対に保証されるものではないからであり、レモ
    ンの代わりとして、みじん切りにしたエシャロットと赤ワインヴィネガー
    を合わせるのも定番的な組み合わせだが、これを白ワインヴィネガーでな
    くてはいけないと主張する向きは少ないだろう。
    
[^7]: 高級なものであれば認めてもいいという安易な主観が入りこんでいる可
    能性もあるだろう。

逆に、ランチでは冷製オードブルをお出しするのが伝統であり、たんに必要と
いうばかりかむしろ欠かせないものだ。

ランチのオードブルはいろいろなものを組みあわせ、エレガントで正確な盛り
付けをすることで価値が高まり、食卓を華やかにして、お客様が食事室に入ら
れてすぐに好印象をあたえてくれる[^6]。

[^6]: ランチ、午餐でポタージュが献立に入らないことが前提になっている。
    エスコフィエは既製品の使用にかならずしも否定的ではないとはいえ、冷
    製オードブルにおける既製品の多用などは現代日本の読者が目を疑うほど
    のものであり、エスコフィエがオードブルをやや低く見ていたことは事実
    だろう。


オードブルが作り方の面でもプレゼンテーションの面でも多いに進歩した結果、
こうした変化がもたらされた。とはいえオードブルはやはり料理芸術の一部と
はいえないだろう。料理芸術は近年、目に見えて大きく進歩しているのだから。

これらオードブルのバリエーションは無限といっていいほどあり、大体でさえ
その組み合わせの数をいうことはできないだろう。その組み合わせは才ある職
人がオードブルにおいて既製品の使い方のじつに多くのバリエーションを考え
だしたものだ。

だから、厨房のチームに優秀なオードブル担当者が必要だと声を大にしていう
べきだ。この部門は二次的なものに見えはするが、\ruby{類稀}{たぐいまれ}
な能力、それは滅多なことでは出来るものではない、確かで鋭い味覚、\ruby{進取}{しんしゅ}の
精神、芸術的完成、そしてプロフェッショナルな知識が必要不可欠だからだ。











<!--20190824江畑校正確認スミ-->


























